\ifdefined\COMPLETE
\else
    \input{./preambule-sacha-utf8.ltx}   
    \usepackage{variations}
    \begin{document}
\fi

\setcounter{theoreme}{0}

\chapter{Une première approche des groupes cristallographiques par les cristaux géométriques}

\section{Groupes cristallographiques : définitions et premières propriétés}

\begin{definition} Un \textit{réseau} est un sous-groupe $L \subseteq E$ tel que $L = \Z v_1 + \Z v_2$, où $\lb v_1, v_2 \rb$ est une base de $E$. 
\end{definition}

\begin{figure}[h]
{\hspace*{5cm}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1cm,y=1cm,scale=1,xslant=.25,rotate=0]

\tikzstyle{noeud}={}
\tikzstyle{lien}=[->,>=latex,very thick]
\tikzstyle{num}=[midway,above]
\tikzstyle{etiquette}=[sloped, midway,above]

\foreach \b in {0, 1, 2, 3}   
	\draw (-0.5,\b) -- (3.5,\b) ; 
\foreach \b in {0, 1, 2, 3} {
\draw (\b,-0.5) -- (\b,3.5) ; 
	\foreach \i in {0,1,2,3} 
		\draw [fill=white](\b,\i) circle (0.05) ; 
}
\draw [-open triangle 45] (0,0) --  node  [midway, below] {$v_1$}(1,0) ; 
\draw [-open triangle 45] (0,0) --  node  [midway, left] {$v_2$}(0,1) ; 


\end{tikzpicture}}
\caption{Exemple de réseau}
\end{figure}

\begin{definition}
Un \textit{groupe cristallographique} est un sous-groupe $G \subseteq \mathrm{Isom}\left(\mathcal{E}\right)$ tel que $$\mathcal{T}\left(G\right) = \enstq{t_v}{t_v \in G} = \enstq{t_v}{v \in L}$$ pour un réseau $L \subseteq E$. 
\end{definition}

\begin{exemple}
Si $M \subseteq \mathcal{E}$ est un « motif périodique dans $2$ directions indépendantes », alors $$\mathrm{Sym}\left(M\right) = \enstq{g \in \mathrm{Isom}\left(\mathcal{E}\right)}{g\left(M\right) = M}$$ est un groupe cristallographique.
\end{exemple}

\section{Sous-groupes finis de $\mathcal{O}\left(E\right)$}

\begin{lemme}(voir \cite{Internet})\label{lemme_fini}
Soit $E$ un espace euclidien, et soit $L$ un réseau de $E$. Alors tout ensemble borné de $E$ contient un nombre fini de points de $L$.
\end{lemme}

\begin{proof}
Soit $\left(e_1, \ldots, e_n\right)$ une base de $L$ (i.e. une base de $E$ qui engendre $L$). Pour tout $x \in E$, notons $Q\left(x\right) = \left(\somme{i=1}{n}{x_i^2}\right)^{\frac{1}{2}}$, où les $x_i$ sont les coefficients de $x$ sur les $\left(e_i\right)_{i \in \lb 1, \ldots, n\rb}$. Montrons que $Q$ est une norme : \\

\begin{itemize}
\item[(Définie positive)] 
Il est clair que, quel que soit $x \in E$, $Q\left(x\right)$ est positive. 

On suppose $Q\left(x\right) = 0$. Alors $\left(\somme{i=1}{n}{x_i^2}\right)^{\frac{1}{2}} = 0$, i.e. $\somme{i=1}{n}{x_i^2} = 0$. \\
Donc pour tout $i \in \llbracket 1, n\rrbracket, x_i = 0$, et ainsi $x = 0$, ce que l'on voulait démontrer. \\

\item[(Homogénéité)] Soit $\lambda \in \R$. \\

\begin{tabular}{llll}
\hspace{-.3cm}On a $Q\left(\lambda x\right)$ & $=$ & $\left
(\somme{i=1}{n}{\left(\lambda x_i\right)^2}\right)^{\frac{1}{2}}$ \\
& $=$ & $\left(\somme{i=1}{n}{\lambda^2 x_i^2}\right)^{\frac{1}{2}}$ \\
& $=$ & $\left(\lambda^2 \; \somme{i=1}{n}{x_i}\right)^{\frac{1}{2}}$ & par linéarité de la somme \\
& $=$ & $\abs{\lambda}\left(\somme{i=1}{n}{x_i^2}\right)^{\frac{1}{2}}$ \\
& $=$ & $\abs{\lambda}Q\left(x\right)$ \\
\end{tabular}

\vspace*{.3cm}

Ainsi $Q$ est bien homogène. \\

\item[(Inégalité triangulaire)] On a : \\

\begin{tabular}{llll}
\hspace*{-.3cm} $Q\left(x+y\right)^2$ & $=$ & $\somme{i=1}{n}{\left(x_i + y_i\right)^2}$ \\
& $=$ & $\somme{i=1}{n}{\left(x_i^2 + y_i^2 + 2x_iy_i\right)}$ \\
& $=$ & $\somme{i=1}{n}{x_i^2} + \somme{i=1}{n}{y_i^2} + 2\somme{i=1}{n}{x_iy_i}$ & par linéarité de la somme \\
& $\leqslant$ & $\somme{i=1}{n}{x_i^2} + \somme{i=1}{n}{y_i^2} + 2\somme{i=1}{n}{\abs{x_iy_i}}$ \\
& $\leqslant$ & $\somme{i=1}{n}{x_i^2} + \somme{i=1}{n}{y_i^2} + 2\left(\somme{i=1}{n}{x_i^2}\right)^{\frac{1}{2}}\left(\somme{i=1}{n}{y_i^2}\right)^{\frac{1}{2}}$ & par l'inégalité \ref{Cauchy_Schwarz} \\
& $\leqslant$ & $Q\left(x\right)^2 + Q\left(y\right)^2 + 2Q\left(x\right)Q\left(y\right)$ \\
& $\leqslant$ & $\left(Q\left(x\right) + Q\left(y\right)\right)^2$ \\
\end{tabular}

\vspace*{.3cm} 

Ainsi $Q$ vérifie bien l'inégalité triangulaire.
\end{itemize} 

\newpage

On a donc montré que $Q$ est une norme, et comme $E$ est de dimension finie, $Q$ est équivalente à la norme euclidienne sur $E$. Ainsi, toute partie bornée de $E$ est bornée pour $Q$, donc il existe $r > 0$ tel que pour tout $x \in L \cap V$, $$Q\left(x\right)^2 = \somme{i=1}{n}{m_i^2} \leqslant r.$$

Mais les $m_i$ sont entiers, d'où le résultat attendu.
\end{proof}

\begin{proposition}(voir \cite{Internet}) \label{Sous_groupes_O2}
Si $G$ est un groupe cristallographique, alors $$\overrightarrow{G} = \enstq{\overrightarrow{f}}{f \in G} \subseteq \mathcal{O}\left(E\right)$$ est un sous-groupe fini.
\end{proposition}

\begin{definition}
$\overrightarrow{G}$ est appelé le \textit{groupe ponctuel} de $G$.
\end{definition}

\begin{proof}
Soit $L$ un réseau de $E$. On considère $\mathcal{O}\left(L\right)$ le groupe des automorphismes orthogonaux de $E$ qui préservent $L$. Si $B$ est une boule de centré en $0$ et de rayon fini qui contient une base de $L$, on constate alors, puisque tout automorphisme orthogonal conserve la norme, qu'il y a un nombre fini de choix pour les images respectives des éléments de cette base (car ces images sont dans $L$ et dans $B$, et $L \cap B$ est fini). \\

Ainsi, $\mathcal{O}\left(L\right)$ est borné, et d'après \ref{lemme_fini}, $\mathcal{O}\left(L\right)$ est fini. \\

Enfin, puisque $\overrightarrow{G}$ laisse stable un réseau $L$, il est contenu dans le groupe $\mathcal{O}\left(L\right)$, et puisqu'on a montré que $\mathcal{O}\left(L\right)$ est fini, on en conclut $\overrightarrow{G}$ est fini.
\end{proof}

\begin{definition}
Le \textit{groupe des rotations} $C_n$ est un groupe cyclique formé de toutes les rotations autour d'un point fixe par des multiples de l'angle $\frac{360^\circ}{n}$.
\end{definition}

\begin{definition}
On appelle \textit{groupe diédral d'ordre $n$}, noté $D_n$ pour $n \geqslant 2$ un groupe d'ordre $2n$ qui s'interprète  comme le groupe des isométries du plan conservant un polygone régulier à $n$ côtés. Le groupe est constitué de $n$ éléments correspondant aux rotations et $n$ autres correspondant aux réflexions.
\end{definition}

\begin{proposition}(voir \cite{Algebra})\label{Sous_groupes_O2}
Les sous-groupes finis $\overrightarrow{G} \subseteq \mathcal{O}\left(E\right)$, à conjugaison près, sont les sous-groupes $C_n$, $n \geqslant 1$, et $D_n$, $n \geqslant 1$.
\end{proposition}

\begin{proof}
On rappelle que les éléments de $\mathcal{O}\left(E\right)$ sont des rotations $\rho$ ou des symétries $s$. \\

\begin{itemize}
\item[•] Supposons que tous les éléments de $\overrightarrow{G}$ soient des rotations. Nous allons prouver qu'alors $\overrightarrow{G}$ est cyclique (la preuve est similaire à celle de la détermination des sous-groupes de $\left(\Z, +\right)$). \\

\begin{itemize}
\item Si $\overrightarrow{G} = \lb \mathrm{Id} \rb$, alors $\overrightarrow{G} = C_1$ ;
\item Sinon $\overrightarrow{G}$ contient une rotation $\rho_\theta$ non triviale. Notons $\theta$ le plus petit angle de rotations parmi les éléments de $\overrightarrow{G}$. Alors $\overrightarrow{G}$ est généré par $\rho_\theta$. \\

Soit $\rho_\alpha \in \overrightarrow{G}$ un élément de $\overrightarrow{G}$ distinct de $\rho_{\theta}$, où $\alpha \in \R$. Notons $n\theta$ le plus grand entier multiple de $\theta$ inférieur à $\alpha$. Alors $\alpha = n\theta + \beta$, où $0 \leqslant \beta < \theta$. \\

Puisque $G$ est un groupe et $\rho_{\alpha}$ et $\rho_{\theta}$ sont des éléments de $G$, alors $\rho_\beta = \rho_\alpha \rho_{-n\theta}$ est un élément de $G$. Or, on a supposé que $\theta$ est le plus petit angle de rotation de $\overrightarrow{G}$. Ainsi $\beta = 0$ et donc $\alpha = n\theta$. On a donc montré que $\overrightarrow{G}$ est cyclique. \\

Enfin, si on suppose que $n\theta$ est le plus petit multiple de $\theta$ tel que $n\theta \geqslant 2\pi$, i.e. $2\pi \leqslant n\theta \leqslant 2\pi + \theta$. Puisque $\theta$ est le plus petit angle de rotation des éléments de $\overrightarrow{G}$, on a $n\theta = 2\pi$. Il vient $\theta = \dfrac{2\pi}{n}$, où $n \in \N$.
\end{itemize}

\item[•] Supposons que $\overrightarrow{G}$ contienne au moins une symétrie, notée $r'$. On peut tourner le système de coordonnées de telle façon que ce soit la symétrie $r$ par rapport à l'axe $\left(Ox\right)$. Notons $H$ le sous-groupe des rotations de $\overrightarrow{G}$. On va montrer que $G = D_n$ par double inclusion. \\

\begin{itemize}
\item[$\left(\supseteq\right)$]  D'après le point précédent, $H$ est un groupe cyclique tel que $H = C_n$, pour $n \geqslant 1$. Pour tout $i \in \llbracket 0, n-1 \rrbracket$, les $\rho_{\theta}^i$ et $r\rho_{\theta}^i$ sont des éléments de $\overrightarrow{G}$, et donc $\overrightarrow{G}$ contient le groupe diédral $D_n$. \\

\item[$\left(\subseteq\right)$] 

\begin{itemize}
\item Si $g \in \overrightarrow{G}$ une rotation. Alors $g \in H$ par définition de $H$. Alors $g \in D_n$. \\

\item Si $g$ est une symétrie, on peut l'écrire sous la forme $r\rho_\alpha$ pour une rotation $\rho_{\alpha} \in \overrightarrow{G}$. Puisque $r \in \overrightarrow{G}$ et $\rho_{\alpha} rr \in \overrightarrow{G}$, alors $\rho_\alpha$ est une puissance de $\rho_{\theta}$, et donc $g \in D_n$.
\end{itemize}

\end{itemize}

\vspace*{.3cm}

On en conclut que $G = D_n$, pour $n \geqslant 1$. 
\end{itemize}
\end{proof}

\newpage

\section{Classification des cristaux géométriques}

On s'intéresse maintenant aux sous-groupes finis de $\mathcal{O}\left(E\right)$, à conjugaison près, qui stabilisent un réseau : 

\begin{proposition}\textbf{(Restriction cristallographique)} (voir \cite{Internet}) On a dix classes de conjugaison de sous-groupes finis de $\mathcal{O}\left(E\right)$ stabilisant un réseau : \\

\begin{itemize}
\item[•] $\overrightarrow{G} = \lb \mathrm{Id}_E\rb$ (i.e. si $G = \enstq{t_v}{v \in L}$, où $L$ est un réseau). \\

\item[•] Le groupe diédral $D_1$ (qui est isomorphe à $\Z/2$).

\begin{figure}[h]
\begin{center}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=1]


\draw (-2,0)-- (5,0);
% \draw (0,-1) -- (0,1) ; 


% Attention de ne pas mettre d'espace dans les parenthèses des symboles
\path (0 ,0 ) coordinate (src) ;
\path (1.5 ,0 )  coordinate (dst) ;
\path (0.25 ,0.25 )  coordinate (ctrl1) ;
\path (1.5 ,0.75)  coordinate (ctrl2) ;
\path (0.25,-0.25 )  coordinate (ctrl3) ;
\path (1.5,-0.75)  coordinate (ctrl4) ;

\draw (src) ..
controls (ctrl1) and (ctrl2) ..
% node [ at start, below , sloped ] {debut}
% node [midway , sloped ] { milieu }
% node [ very near end , above , sloped ] { f i n }
% (dst) ;
(dst) ; 
\draw (src) ..
controls (ctrl3) and (ctrl4) ..
(dst) ; 

\draw[fill=white](0,0) circle (0.15) ; 

\newcommand{\AxisRotator}[1][rotate=0]{%
 \tikz [x=0.25cm,y=0.60cm,line width=.1ex,-stealth,#1]
\draw (0,0) arc (-150:150:1 and 1);%
}
\draw (2,0)  -- (3,0)  node [midway] {\AxisRotator};
\draw (4,0) node [below]{$\mathcal{D}$} ; 

\end{tikzpicture}
\end{center}
\caption{Groupe diédral $D_1$ (stabilisant le monogone)}
\end{figure} 

\item[•] Le groupe diédral $D_2$ qui est isomorphe à $\Z/2 \times \Z/2$.

\begin{figure}[h]
\begin{center}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=1]

\draw (-2,0)-- (5,0);
\draw (1,-1) -- (1,1) ; 

\path (0 ,0 ) coordinate (src) ;
\path (2 ,0 )  coordinate (dst) ;
\path (0.25 ,0.5 )  coordinate (ctrl1) ;
\path (1.5 ,0.5)  coordinate (ctrl2) ;
\path (0.25,-0.5 )  coordinate (ctrl3) ;
\path (1.5,-0.5)  coordinate (ctrl4) ;

\draw (src) ..
controls (ctrl1) and (ctrl2) ..
(dst) ; 
\draw (src) ..
controls (ctrl3) and (ctrl4) ..
(dst) ; 

\draw[fill=white](0,0) circle (0.15) ; 
\draw[fill=white](2,0) circle (0.15) ; 

\newcommand{\AxisRotator}[1][rotate=0]{%
\tikz [x=0.25cm,y=0.60cm,line width=.1ex,-stealth,#1]
\draw (0,0) arc (-150:150:1 and 1);%
}
\draw (2,0)  -- (4,0)  node [midway] {\AxisRotator[<->,line width=.1ex, stealth-stealth ]};
\draw (1,0)  -- (1,2)  node [midway,rotate=270] {\AxisRotator[<->,line width=.1ex, stealth-stealth ]};

\draw (4,0) node [below]{$\mathcal{D}$} ; 
 
\draw (1,2) node [right]{$\mathcal{D}'$} ; 

\end{tikzpicture}
\end{center}
\caption{Groupe diédral $D_2$ (stabilisant le digone)}
\end{figure}

\item[•] Le groupe diédral $D_3$ qui est isomorphe à $\mathfrak{S}_3$.

\vspace*{-1.8cm}

\begin{figure}[h]
\begin{center}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=1.2]

\newcommand{\AxisRotator}[1][rotate=0]{%
\tikz [x=0.10cm,y=0.40cm,line width=.1ex,-stealth,#1]
\draw (0,0) arc (-160:160:1 and 1);%
}

% \draw (2,0)  -- (4,0)  node [midway] {\AxisRotator[<->,line width=.1ex, stealth-stealth ]};

\draw (210:1.5) --  (330:1.5) -- (0,1.5)  -- (210:1.5) -- cycle ;
\draw (0,-1.5)-- (0,2.5);
\draw (210:2.25) -- (30:1.5) ;  
\draw (330:2.25) -- (150:1.5) ;  

\draw (-0.6,2.2)  node {$s_2$} ; 
\draw (0,1.5) -- (0,2.5) node [midway,rotate=90] {\AxisRotator[<->,line width=.1ex, stealth-stealth ]};

\draw (40:1.65)  node {$s_1$} ; 
\draw (60:2)  node {$\rho_1$} ; 
\draw (210:-1) -- (30:1.5)   node [midway,rotate=30] {\AxisRotator[<->,line width=.1ex, stealth-stealth ]};


\draw  (160:1.65)   node {$s_3$} ; 
\draw (330:-1) -- (150:1.5)  node [midway,rotate=150] {\AxisRotator[<->,line width=.1ex, stealth-stealth ]};

\draw  (300:1.5)   node {$\mathrm{Id} = \rho^3 $} ; 
\fill  (330:1.45) circle (0.05); 

\draw(330:1.5) -- (150:-1)  node [midway,rotate=150] {\AxisRotator[line width=.1ex ]};

 \draw  [->,>=latex,x=1cm,y=1cm,](330:1.45) .. controls  (2,0) and (2,2) ..   (0,1.5)  ;

\draw  (2.25,2.25)   node {$\rho_2=\rho^{-1} $} ; 
 \draw  [->,>=latex,x=1cm,y=1cm] (330:1.45)  .. controls  (4.5,3) and (-4,5) ..  (210:2)  ;

\end{tikzpicture}
\end{center}
\caption{Groupe diédral $D_3$ (stabilisant le triangle)}
\end{figure}

\vspace*{-50cm}

\newpage

\begin{samepage}

\item[•] Le groupe diédral $D_4$. 

\vspace*{-5.3cm}

\begin{figure}[!h]
\begin{center}
\hspace*{2.5cm}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=1.2]
\newcommand{\AxisRotator}[1][rotate=0]{%
\tikz [x=0.10cm,y=0.40cm,line width=.1ex,-stealth,#1]
\draw (0,0) arc (-160:160:1 and 1);%
}

\draw (1,-1)  -- (1,1) -- (-1,1) -- (-1,-1)   -- cycle ; 


% Verticale 
\draw(0,-1.5)-- (0,1.5) ; 
\draw (-0.25,1.5)  node {$s_3$} ; 
\draw (0,1) -- (0,1.5) node [midway,rotate=90] {\AxisRotator[<->,line width=.1ex, stealth-stealth ]};

% Horizontale 
\draw (-1.4,0)-- (2,0) ; 
\draw (2,0.25)  node {$s_1$} ; 
\draw (1.7,0) -- (1.7,0)   node [midway,rotate=0] {\AxisRotator[<->,line width=.1ex, stealth-stealth ]};


% Oblique SW to NE
\draw  (225:2)-- (45:2); 
\draw (45:1.9) node[above] {$s_2$} ; 
\draw (45:1.74) -- (45:1.75)   node [midway,rotate=45] {\AxisRotator[<->,line width=.1ex, stealth-stealth ]};


% Oblique NW to SE
\draw  (135:2)-- (315:2); 
\draw (135:1.9) node[above] {$s_4$} ; 
\draw (135:1.74) -- (135:1.75)   node [midway,rotate=135] {\AxisRotator[<->,line width=.1ex, stealth-stealth ]};


\draw  (0.8,-1.5)   node {$\mathrm{Id} = \rho ^4 $} ; 
\fill  (1,-1) circle (0.05); 
\draw(315:1.25) -- (315:1.3)  node [midway,rotate=150] {\AxisRotator[line width=.1ex ]};

\draw  (2.5,0.25)   node {$\rho^1 $} ; 
 \draw  [->,>=latex,x=1cm,y=1cm,](1,-1)  .. controls  (2.5,-0.75) and (2.75,0.75) ..   (45:2)   ;

\draw  (2.85,1)   node {$\rho^2 $} ; 
 \draw  [->,>=latex,x=1cm,y=1cm,](1,-1)  .. controls  (4.25,-.75) and (2.25,4.25) ..   (135:1.5)   ;

\draw  (3.6,0)   node {$\rho^3 $} ; 
 \draw  [->,>=latex,x=1cm,y=1cm,](1,-1)  .. controls  (8.5,-.25) and (120:8) ..   (225:1.75)   ;

\end{tikzpicture}
\end{center}
\caption{Groupe diédral $D_4$ (stabilisant le carré)}
\end{figure}

\item[•] Le groupe diédral $D_6$ (stabilisant l'hexagone). \\

\item[•] Le groupe $C_2$.

\vspace{-.75cm}

\begin{figure}[!h]
\begin{center}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=2]

\draw (0,0) -- (.95,0) ;

\fill  (0,0)  circle (0.05) ; 
\draw (1,0)  circle (0.05) ; 

\draw  (0.6,0.5)  node {$\rho$} ; 
\draw [->,>=latex,x=1cm,y=1cm,] (0,0) .. controls (-.25 ,0.45) and  (1 ,0.45)  ..  (1,0.05) ; 
\newcommand{\AxisRotator}[1][rotate=0]{%
\tikz [x=0.10cm,y=0.40cm,line width=.1ex,-stealth,#1]
\draw (0,0) arc (175:-175:1 and 1);%
}

\draw  (-.6,0 )  node {$\rho^2 = \mathrm{Id}$} ; 
\draw (-.1,0) -- (-0.1,0)   node [midway,rotate=180] {\AxisRotator[line width=.1ex, stealth- ]};
\fill  [white](-0.1,0)  circle (0.02) ; 


\end{tikzpicture}
\caption{Groupe $C_2$}
\end{center}
\end{figure}

\item[•] Le groupe $C_3$.

\vspace*{-1.75cm}

\begin{figure}[!h]
\begin{center}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=2]

\path (0,0 ) coordinate (src) ;
\path (1,0.05 )  coordinate (dst) ;

\draw (0.05,0) -- (1,0) ;

\fill  (1,0)  circle (0.05) ; 
\draw (0,0)  circle (0.05) ; 
\draw (0.5,0.75)  circle (0.05) ; 
\draw (57:0.05) -- (57:0.85) ;
\draw (1.05,0.7)   node {$\rho$} ;
\draw (1,0) --  (.54,0.7) ;
\draw [->,>=latex,x=1cm,y=1cm,] (1,0) .. controls (1,0.8) and (0.85,0.85) .. (54:0.9) ;

\draw (0,1.1)   node {$\rho^2$} ;
\draw [->,>=latex,x=1cm,y=1cm,] (1,0) .. controls (2,2) and (-1,1) .. (-0.025,0.025) ;


\draw (1.6,0)   node {$\rho^3=\mathrm{Id}$} ;
\newcommand{\AxisRotator}[1][rotate=0]{%
\tikz [x=0.10cm,y=0.30cm,line width=.1ex,stealth-,#1]
\draw (0,0) arc (-165:170:1 and 1) ;
}

\draw (1.05,-0.05) -- (1.05,-0.05)  node [midway, rotate=315] {\AxisRotator[line width=.1ex ]};
\fill  [white](1.05,-0.05) circle (0.02) ; 

\end{tikzpicture}
\end{center}
\caption{Groupe $C_3$}
\end{figure} 

\item[•]Le groupe $C_4$.

\vspace*{-1.2cm}

\begin{figure}[H]
\begin{center}
\hspace*{2cm}
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=2]

\draw (0,0) -- (1,0) -- (1,1) -- (0,1) -- cycle ; 

\fill  (1,0)  circle (0.05) ;  \fill [white] (1,1)  circle (0.05) ;  \fill [white] (0,1)  circle (0.05) ;   \fill [white] (0,0)  circle (0.05) ; 
\draw (0,0)  circle (0.05) ; 
\draw (0,1)  circle (0.05) ; 
\draw (1,1)  circle (0.05) ; 

\draw (1.35,0.7)   node {  $\rho$} ;
\draw [->,>=latex,x=1cm,y=1cm,] (1,0) .. controls (1.25,0.25) and (1.35,0.75) .. (1.05,1) ;

\draw (0.05,1.25)   node {  $\rho^2$} ;
\draw [->,>=latex,x=1cm,y=1cm,] (1,0) .. controls (2.5,0.75) and (0.5,1.75) .. (0.025,1.025) ;

\draw (0.2,-0.45)   node {  $\rho^{-1} = \rho^3$} ;
\draw [->,>=latex,x=1cm,y=1cm,] (1,0) .. controls (0.85,-0.35) and (0.15,-0.35) .. (0,-0.05) ;

\draw (1.6, -0.25)   node {$\rho^4 = \mathrm{Id}$} ;
\draw [<-,>=latex] (1,-0.05) .. controls (1.1,-.5) and (1.5,-.25) .. (1,0) ;

\end{tikzpicture}
\end{center}
\caption{Groupe $C_4$}
\end{figure} 


\item[•] Le groupe $C_6$.

\end{samepage}

\vspace*{-50cm}

\newpage

\end{itemize}
\end{proposition}

\begin{definition}
Ces dix classes de conjugaison des sous-groupes finis de $\mathcal{O}\left(E\right)$ sont appelés \textit{cristaux géométriques}.
\end{definition}

\begin{proof} Soit $G$ un groupe cristallographique. 
D'après la propriété \ref{Sous_groupes_O2}, $\overrightarrow{G}$ est un sous-groupe fini de $\mathcal{O}\left(E\right)$ égal à $D_n$ ou $C_n$, pour un certain $n \in \N^*$. 

Soit $n \in \N$.
Par définition, les groupes $D_n$ et $C_n$ contiennent une rotation d'angle $\theta = \dfrac{360}{n}$. 

\vspace*{.1cm}

La matrice de cette rotation dans une base de $E$ est donnée par $M_1 = \begin{pmatrix}
\cos \theta & -\sin \theta \\
\sin \theta & \cos \theta \\
\end{pmatrix}$. \\

On considère maintenant une base du réseau. Par hypothèse, le réseau est laissé stable par la rotation, donc les vecteurs du réseau sont envoyés sur des vecteurs du réseau, i.e. sur des combinaisons linéaires entières de vecteurs de vecteurs de la base. Alors la matrice $M_2$ de la rotation dans une base du réseau fixée est $M_2 = \begin{pmatrix}
a & b \\
c & d \\
\end{pmatrix}$, où $\left(a,b,c,d\right) \in \Z^4$. \\

En particulier, on a $\mathrm{Tr}\left(M_1\right) = 2\cos \theta$ et $\mathrm{Tr}\left(M_2\right) = a + d$, i.e. $\mathrm{Tr}\left(M_2\right) \in \Z$. \\
On rappelle que la trace d'une matrice est invariante par changement de base. \\ Donc $\mathrm{Tr}\left(M_1\right) =\mathrm{Tr}\left(M_2\right)$, et ainsi $2\cos \theta \in \Z$. \\

Soit $m \in \Z$. On pose $2\cos \theta = m$.

Puisque $-1 \leqslant \cos \theta \leqslant 1$, on a $-2 \leqslant m \leqslant 2$, i.e. $m \in \lb -2, -1, 0, 1, 2\rb$. Il vient : 

\renewcommand{\arraystretch }{1.5}

\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
$m$ & $-2$ & $-1$ & $0$ & $1$ & $2$ \\
\hline
$\cos \theta$ & $-1$ & $-\frac{1}{2}$ & $0$ & $\frac{1}{2}$ & $1$ \\
\hline
$\theta$ & $180^{\circ}$ & $\pm 120^{\circ}$ & $\pm 90^{\circ}$ & $\pm 60^{\circ}$ & $360^{\circ}$ \\
\hline
$n$ & $2$ & $3$ & $4$ & $6$ & $1$ \\ 
\hline
\end{tabular}
\end{center}

\renewcommand{\arraystretch }{1}

\vspace*{.3cm}

D'où $n \in \lb 1, 2, 3, 4, 6\rb$. Donc les sous-groupes finis $\overrightarrow{G} \subseteq \mathcal{O}\left(E\right)$ stabilisant le réseau sont, à conjugaison près, les sous-groupes $C_n$ et $D_n$, pour $n \in \lb 1, 2, 3, 4, 6\rb$.
\end{proof}


\ifdefined\COMPLETE
\else
    \end{document}
\fi

