\ifdefined\SUBCOMPLETE
\else
    \input{./preambule-sacha-utf8.ltx}   
    \usepackage{variations}
	\begin{document}
\fi

\chapter{Rappels d'algèbre linéaire et de théorie des groupes}

\section{Espace euclidien}

\begin{definition} Soit $E$ un $\R$-espace vectoriel. Soit $\langle \cdot, \cdot \rangle : E \times E \longrightarrow \R$. On dit que $\langle \cdot, \cdot \rangle$ est un \textit{produit scalaire} sur $E$ si : 

\begin{enumerate}
\item (Symétrie) $\forall \left(x,y\right) \in E^2, \langle x, y \rangle = \langle y, x\rangle$.
\item (Bilinéarité) $\forall \left(x,y,z\right) \in E^3, \forall \lambda \in \R, \langle x + \lambda y, z\rangle = \langle x, z\rangle + \lambda \langle y, z\rangle$. 
\item (Positivité) $\forall x \in E, \left\{
  \begin{array}{l}
    \langle x, x\rangle \geqslant 0 \\
    \langle x, x\rangle = 0 \Longleftrightarrow x = 0 \\
  \end{array}
\right.$.
\end{enumerate}

\end{definition} 

\begin{definition} On appelle \textit{espace euclidien} un $\R$-espace vectoriel de dimension finie muni d'un produit scalaire noté $\langle \cdot, \cdot\rangle$. 
\end{definition}

\begin{lemme} \label{identites_remarquables_produit_scalaire}
On a les identités remarquables suivantes : \\

\begin{itemize}
\item[•] $\forall \left(x, y\right) \in E^2, \langle x + y, x + y\rangle = \langle x, x\rangle + 2\langle x, y\rangle + \langle y, y\rangle$. \vspace*{-.1cm} \\
\item[•] $\forall \left(x, y\right) \in E^2, \langle x - y, x - y\rangle = \langle x, x\rangle - 2\langle x, y\rangle + \langle y, y\rangle$. \vspace*{-.1cm} \\
\item[•] $\forall \left(x, y\right) \in E^2, \langle x + y, x - y\rangle = \langle x, x\rangle - \langle y, y\rangle$. \vspace*{-.1cm} \\
\end{itemize}
\end{lemme}

\begin{proof}
Soit $\left(x, y\right) \in E^2$. \\

\begin{tabular}{lll}
\hspace*{-.35cm} $\langle x + y, x + y\rangle$ & $=$ & $\langle x + y, x\rangle + \langle x + y, y\rangle$ par linéarité à droite \\
& $=$ & $\langle x , x\rangle + \langle y, x \rangle + \langle x, y\rangle + \langle y, y\rangle$ par linéarité à gauche \\
& $=$ & $\langle x , x\rangle + \langle x, y \rangle + \langle x, y\rangle + \langle y, y\rangle$ par symétrie \\
& $=$ & $\langle x, x\rangle + 2\langle x, y\rangle + \langle y, y\rangle$ \\
\end{tabular}

\vspace*{.3cm}


\begin{tabular}{lll}
\hspace*{-.35cm} $\langle x - y, x - y\rangle$ & $=$ & $\langle x - y, x\rangle - \langle x - y, y\rangle$ par linéarité à droite \\
& $=$ & $\langle x , x\rangle - \langle y, x \rangle - \langle x, y\rangle + \langle y, y\rangle$ par linéarité à gauche \\
& $=$ & $\langle x , x\rangle - \langle x, y \rangle - \langle x, y\rangle + \langle y, y\rangle$ par symétrie \\
& $=$ & $\langle x, x\rangle - 2\langle x, y\rangle + \langle y, y\rangle$ \\
\end{tabular}

\newpage

\begin{tabular}{lll}
\hspace*{-.35cm} $\langle x + y, x - y\rangle$ & $=$ & $\langle x + y, x\rangle - \langle x + y, y\rangle$ par linéarité à droite \\
& $=$ & $\langle x , x\rangle + \langle y, x \rangle - \langle x, y\rangle - \langle y, y\rangle$ par linéarité à gauche \\
& $=$ & $\langle x , x\rangle + \langle x, y \rangle - \langle x, y\rangle - \langle y, y\rangle$ par symétrie \\
& $=$ & $\langle x, x\rangle - \langle y, y\rangle$ \\

\vspace*{-.8cm}

\end{tabular} ~ \\
\end{proof}

On cherche ensuite à définir la notion de « longueur » d'un vecteur d'un espace euclidien, qu'on appelle la norme d'un vecteur, à partir du produit scalaire que l'on a considéré. Pour cela, on va utiliser le résultat suivant :

\begin{proposition}(Inégalité de Cauchy\footnote{Augustin Louis, baron Cauchy (1789-1857) est un mathématicien français.}-Schwarz\footnote{Hermann Amandus Schwarz (1843-1921) est un mathématicien allemand.}) Soit $E$ un espace euclidien. Alors pour tous vecteurs $x, y \in E$, on a : 

\vspace*{-.3cm}

\begin{equation} \label{Cauchy_Schwarz}
\langle x, y\rangle^2 \leqslant \langle x, x\rangle \langle y, y\rangle.
\end{equation}

\end{proposition}

\begin{proof}
Soit $\left(x, y\right) \in E$. 

\begin{itemize}
\item[•] Supposons $y = 0$. \\
Alors $\langle x, 0\rangle = \langle x, 0 \cdot 0\rangle = 0 \cdot\langle x, 0\rangle = 0$. \\
De plus, $\langle x, x\rangle \langle 0, 0\rangle = 0$, par $\langle \cdot, \cdot\rangle$ est défini positif. \\
Donc l'inégalité \ref{Cauchy_Schwarz} est vrai (on a même égalité). \\

\item[•] Supposons $y \neq 0$. On considère la fonction $f : t \longmapsto f(t) = \langle x + ty, x + ty\rangle$. \\
On a clairement que $f(t) \geqslant 0 (*)$, par positivité du produit scalaire. \\

De plus, on a : 

\vspace*{-1cm}

\begin{align*}
f(t) & = \langle x + ty, x + ty\rangle \\
& = \langle x + ty, x\rangle + t\langle x + ty, y\rangle \\
& = \langle x, x\rangle + t\langle y, x\rangle + t\langle x, y\rangle + t^2\langle y, y\rangle \\
& = \langle x, x\rangle + t\langle x, y\rangle + t\langle x, y\rangle + t^2\langle y, y\rangle \\
& = \langle x, x\rangle + 2t\langle x, y\rangle + t^2\langle y, y\rangle \\
\end{align*}

\vspace*{-.5cm}

On a supposé $y \neq 0$, donc $t^2\langle y, y\rangle \neq 0$. Donc $f$ est bien une fonction polynôme de degré $2$. On calcule son discriminant : $$\Delta = b^2 - 4ac = 4\langle x, y\rangle^2 - 4\langle x, x\rangle\langle y, y\rangle = 4\left(\langle x, y\rangle^2 - \langle x, x\rangle\langle y, y\rangle\right).$$ 

Or, d'après $\left(*\right)$, $f$ ne change pas de signe. On en déduit que son discriminant est négatif. Ainsi $\Delta < 0$, i.e. $4\left(\langle x, y\rangle^2 - \langle x, x\rangle\langle y, y\rangle\right) < 0$, i.e. $\langle x, y\rangle^2 - \langle x, x\rangle\langle y, y\rangle$, i.e. $\langle x, y\rangle^2 \leqslant \langle x, x\rangle\langle y, y\rangle$, ce qu'on voulait démontrer. \\

\textit{Cas d'égalité :} Puisqu'on a supposé $y \neq 0$, $x$ et $y$ sont liés si, et seulement si, il existe $\lambda \in \R$ tel que $y = \lambda x$. Si c'est le cas, un calcul analogue au développement de $f(t)$ nous donne l'égalité attendue. \\
Réciproquement, s'il y a égalité dans (\ref{Cauchy_Schwarz}), alors le discriminant de $f(t)$ est nul, i.e. $f(t)$ admet une racine, i.e. il existe $t_0 \in \R$ tel que $f\left(t_0\right) = 0$. On obtient $\langle x + t_0y, x + t_0y\rangle = 0$, et par positivité de $\langle \cdot, \cdot\rangle$, $x + t_0y = 0$, donc $x$ et $y$ sont liés. 
\end{itemize}
\end{proof}

\vspace*{-50cm}

\newpage

La notion intuitive de longueur d'un vecteur est donc donnée mathématiquement par la notion de norme, définie par : 

\vspace*{.1cm}

\begin{definition}
Soit $E$ un $\R$-espace vectoriel. Une fonction $\norme{\cdot} : E \longrightarrow \R$ est appelée \textit{norme} sur $E$ si elle vérifie les trois propriétés suivantes : 

\begin{enumerate}
\item (Positivité) $\forall x \in E, \left\{
  \begin{array}{l}
    \langle x, x\rangle \geqslant 0 \\
    \langle x, x\rangle = 0 \Longleftrightarrow x = 0 \\
  \end{array}
\right.$.
\item (Homogénéité) $\forall x \in E, \forall \lambda \in \R, \norme{\lambda x} = \abs{\lambda} \cdot \norme{x}$.
\item (Inégalité triangulaire) $\forall \left(x,y\right) \in E^2, \norme{x + y} \leqslant \norme{x} + \norme{y}$.
\end{enumerate}
\end{definition}

\begin{theoreme}
Soit $E$ un $\R$-espace vectoriel, et soit $\langle \cdot, \cdot \rangle$ un produit scalaire sur $E$. \\
Alors la fonction $\norme{\cdot} : E \longrightarrow \R$ définie par $\norme{x} = \sqrt{\langle x, x\rangle}$ est une norme sur $E$.
\end{theoreme}

\begin{proof} On vérifie les trois points de la définition du produit scalaire : \\

\begin{itemize}
\item[•] (Positivité) La positivité de $\norme{\cdot}$ est une conséquence directe de la positivité de $\langle \cdot, \cdot\rangle$.  \\
\item[•] (Homogénéité) Soit $\lambda \in \R$. On a : $$\norme{\lambda x}^2 = \langle\lambda x, \lambda x\rangle = \lambda \langle x, \lambda x\rangle = \lambda^2 \langle x, x\rangle = \lambda^2 \norme{x}.$$
Ainsi, en passant aux racines carrées, on obtient bien $\norme{\lambda x} = \abs{\lambda}\cdot \norme{x}$. \\
\item[•] (Inégalité triangulaire) Soit $\left(x,y\right) \in E^2$. \\
On veut vérifier que $\norme{x + y} \leqslant \norme{x} + \norme{y}$, donc $\norme{x + y}^2 \leqslant \left(\norme{x} + \norme{y}\right)^2$.

En développant par les identités remarquables \ref{identites_remarquables_produit_scalaire}, on obtient : \\

\begin{tabular}{lll}
\hspace*{-.35cm} $\norme{x + y}^2$ & $=$ & $\norme{x}^2 + 2\langle x, y\rangle + \norme{y}^2$ \\
& $\leqslant$ & $\norme{x}^2 + 2\abs{\langle x, y\rangle} + \norme{y}^2$ \\
& $\leqslant$ & $\norme{x}^2 + 2\norme{x}\norme{y} + \norme{y}^2$ d'après l'inégalité \ref{Cauchy_Schwarz} \\
& $=$ & $\left(\norme{x} + \norme{y}\right)^2$. \\
\end{tabular}
\end{itemize}
\end{proof}

\begin{definition}
Deux vecteurs $x$ et $y$ de $E$ sont dits \textit{orthogonaux} si $\langle x, y\rangle = 0$. 
\end{definition}

\begin{definition} Soit $k \in \N^*$. \\
Un \textit{système} $\left(x_i\right)_{1 \leqslant i \leqslant k}$ est \textit{orthonormé} si, pour tout $\left(i,j\right) \in \llbracket 1, k\rrbracket^2, i \neq j$, on a $\langle x_i, x_j\rangle = 0$ et tous les vecteurs $x_i$ sont de norme $1$. \\

En particulier, une \textit{base} $e = \left(e_1, \cdots, e_n\right)$ \textit{de $E$ est orthonormée} si, et seulement si : $$\forall \left(i,j\right) \in \llbracket 1, n\rrbracket^2, \langle e_i, e_j\rangle = \delta_i^j$$
où $\delta_i^j = 1$ si $i = j$ et $0$ sinon. 
\end{definition}

\begin{theoreme}\textbf{(Théorème du rang)}\label{theoreme_du_rang}
Soient $E$ et $F$ deux espaces vectoriels (de dimension finie ou infinie) sur un corps $K$ et soit $f \in \mathcal{L}\left(E, F\right)$. Alors $$\mathrm{rg} \; f + \dim \ker f = \dim E,$$ où $\mathrm{rg} \; f$ désigne la dimension de l'image de $f$.
\end{theoreme}

\section{Endomorphisme adjoint}

Soit $E$ est un espace vectoriel euclidien. Soit $f : E \longrightarrow E$. 

\begin{proposition}
Il existe un unique endomorphisme $g : E \longrightarrow E$ tel que : $$\forall \left(x, y\right) \in E^2, \langle f\left(x\right), y\rangle = \langle x, g\left(y\right)\rangle.$$
\end{proposition}

\begin{definition}
L'application $g$ de la proposition précédente est appelée \textit{application adjointe}, on la note $f^*$. Ainsi $f^* : E \longrightarrow E$ est l'unique endomorphisme tel que : $$\forall v,w \in E, \langle v, f^*\left(w\right)\rangle = \langle f\left(v\right), w\rangle.$$
\end{definition}

\begin{lemme} \label{matrice_decomposition}
Soit $e = \left(e_1, \cdots, e_n\right)$ une base orthonormée de $E$. Alors tout vecteur $x \in E$ admet la décomposition suivante dans la base $e$ : $$x = \somme{i = 1}{n}{\langle x, e_i\rangle e_i}.$$

En particulier, si $M = \left(M_{i, j}\right)$ est la matrice dans la base $e$ de $f$, alors : 

\begin{equation} \label{matrice_decomp}
M_{i, j} = \langle e_i, f\left(e_j\right)\rangle
\end{equation}
\end{lemme}

\vspace*{-.3cm}

\begin{proof}
Puisque $e$ est une base, il existe une famille $\left(\lambda_1, \cdots, \lambda_n\right) \in \R^n$ telle que $x = \somme{i=1}{n}{\lambda_i e_i}$. En effectuant le produit scalaire avec un vecteur $e_j$ de la base, et en utilisant que $\langle e_i, e_j\rangle = \delta_i^j$, on obtient $\langle x, e_j \rangle = \left\langle \somme{i = 1}{n}{\lambda_i e_i}, e_j\right\rangle = \lambda_i \somme{i=1}{n}{\langle e_i, e_j \rangle} = \lambda_j$. \\

Ainsi, on a bien $x = \somme{i=1}{n}{\langle x, e_i\rangle e_i}$. \\

La relation (\ref{matrice_decomp}) en découle, car le $j$ième vecteur colonne $\left(M_{i,j}\right)_i$ collecte les coordonnées dans la base $e$ de l'image du vecteur de la base $f\left(e_j\right)$.
\end{proof}

\begin{proposition} Supposons que $A$ soit la matrice de $f$ dans une base orthonormée. \\ Alors $\transposee{A}$ est la matrice de $f^*$ dans cette même base. 
\end{proposition}

\begin{proof} Soit $n \in \N$.
Soit $e = \left(e_1, \ldots, e_n\right)$ une base orthonormée de $E$. On suppose que $A$ est la matrice de $f$ dans $e$, et on note $A'$ la matrice de $f^*$ dans $e$. Montrons que $A' = \transposee{A}$. \\

\begin{tabular}{lll}
\hspace*{-.35cm} On a $A'_{i, j}$ & $=$ & $\langle f^*\left(e_i\right), e_j\rangle$ d'après le lemme \ref{matrice_decomposition} \\
& $=$ & $\langle e_i, f\left(e_j\right)\rangle$ par définition de $f^*$ \\
& $=$ & $A_{j, i}$. \\
\end{tabular}

\vspace*{.3cm}

On a donc montré que $A$ et $A'$ sont transposées l'une de l'autre.
\end{proof}

\begin{definition}
Un endomorphisme $u : E \longrightarrow E$ est \textit{auto-adjoint} si $u^* = u$.
\end{definition}

\begin{definition}
Un endomorphisme $u : E \longrightarrow E$ est \textit{symétrique} si $$\forall \left(x,y\right) \in E^2, \langle u\left(x\right), y\rangle = \langle x, u(y) \rangle.$$
\end{definition}

\begin{proposition}\label{auto_adjoint_symetrie}
Si $f$ est auto-adjoint, alors $f$ est symétrique. 
\end{proposition}

\begin{proof}
Si $f$ est auto-adjoint, on a $f^* = f$, et donc, par définition de $f^*$ : $$\forall \left(x,y\right) \in E^2, \langle f(x), y\rangle = \langle x, f^*\left(y\right)\rangle = \langle x, f\left(y\right)\rangle.$$ 
Donc $f$ est symétrique.
\end{proof}

\begin{proposition}\label{f_sym_f_moins_1_sym}
Si $f$ est symétrique, alors $f^{-1}$ est symétrique.
\end{proposition}

\begin{proof}
$f$ est symétrique, donc par hypothèse $$\forall \left(x, y\right) \in E^2, \langle f(x), y\rangle = \langle x, f(y) \rangle$$ 

Soit $\left(x, y\right) \in E^2$. On a : \\

\begin{tabular}{lll}
\hspace*{-.35cm} $\langle f^{-1}\left(x\right), y \rangle$ & $=$ & $\langle f^{-1}\left(x\right), f\left(f^{-1}\left(y\right)\right) \rangle$ \\
& $=$ & $\langle f^{-1}\left(x\right), f\left(f^{-1}\left(y\right)\right) \rangle$ \\
& $=$ & $\langle f\left(f^{-1}\left(x\right)\right), f^{-1}\left(y\right) \rangle$ car $f$ est symétrique \\
& $=$ & $\langle x, f^{-1}\left(y\right) \rangle$ \\
\end{tabular}

\vspace*{.3cm}

Donc $f^{-1}$ est symétrique. 
\end{proof}

\vspace*{.1cm}

\begin{definition}
Un endomorphisme symétrique $u : E \longrightarrow E$ est dit \textit{positif} si : $$\forall x \in E, \langle u\left(x\right), x\rangle \geqslant 0.$$

De plus, on dit que $u$ est \textit{défini positif} si $u$ est positif et $\forall x \in E, \langle u(x), x \rangle = 0 \Longrightarrow x = 0$.
\end{definition}

\vspace*{.1cm}

\begin{proposition} \label{valeurs_propres_positives}
Si $f$ est un endomorphisme de $E$ défini positif, toutes ses valeurs propres sont des réels strictement positifs.  
\end{proposition}

\begin{proof}
Soit $f : E \longrightarrow E$ un endomorphisme symétrique. On suppose que $f$ est défini positif. Soit $\lambda$ une valeur propre de $f$ et $x$ un vecteur propre associé. On a donc $$\langle f\left(x\right), x\rangle = \langle \lambda x, x\rangle = \lambda\langle x, x\rangle = \lambda \norme{x}^2$$

Ainsi $\lambda = \dfrac{\langle f\left(x\right), x\rangle}{\norme{x}^2}$. Or, comme $f$ est défini positif, $\langle f\left(x\right), x\rangle > 0$, et donc $\lambda$ est strictement positif.
\end{proof}

\begin{definition}
Un endomorphisme $u : E \longrightarrow E$ est \textit{orthogonal} si $$\forall \left(x,y\right) \in E^2, \langle u(x), u(y) \rangle = \langle x, y\rangle.$$
\end{definition}

\begin{lemme} \label{rec_orthogonale}
Si $u$ est orthogonal, alors $u^{-1}$ est orthogonal. 
\end{lemme}

\begin{proof} Soit $\left(x, y\right) \in E^2$. Supposons $u$ orthogonal. \\ Alors $\langle u(x), u(y) \rangle = \langle x, y\rangle$, i.e. $\langle u(x), u(y) \rangle = \langle u\left(u^{-1}\left(x\right)\right), u\left(u^{-1}\left(y\right)\right)\rangle$. \\
Alors $\langle x, y\rangle = \langle u^{-1}\left(x\right), u^{-1}\left(y\right) \rangle$, donc $u^{-1}$ est orthogonal.
\end{proof}

\begin{proposition}  \label{inverse_orthogonal} Soit $f : E \longrightarrow E$ un endomorphisme. \\
La propriété précédente est équivalente  $f^*f =  \mathrm{Id}_E$, i.e. à $f^* = f^{-1}$.
\end{proposition}

\begin{proof} On va montrer que $f$ est orthogonal si et seulement si $f^{-1}$ est l'adjoint de $f$, i.e. $$\forall \left(x, y\right) \in E^2, \langle f(x), f(y) \rangle = \langle x, y\rangle \Longleftrightarrow f^* = f^{-1}.$$   

\vspace*{.3cm}

\begin{itemize}
\item[$\left(\Longrightarrow\right)$] Supposons que $f$ est orthogonal, i.e. $\forall \left(x,y\right) \in E^2, \langle f(x), f(y) \rangle = \langle x, y\rangle$, et montrons qu'alors $f^* = f^{-1}$. \\

D'après la propriété \ref{rec_orthogonale}, $f^{-1}$ est orthogonal. Ainsi, pour $\left(x, y\right) \in E^2$, on obtient, en composant à gauche par $f^{-1}$ : 
$$ \langle x, f\left(y\right) \rangle = \langle f^{-1}\left(x\right), y\rangle.$$

Or, cette relation caractérise l'adjoint de $f$. Donc $f^{-1} = f^*$. \\
 
\item[$\left(\Longleftarrow\right)$] Supposons $f^{-1} = f^*$. Alors : \\

\begin{tabular}{lll}
\hspace*{-.35cm} $\langle f\left(x\right), f\left(x\right) \rangle$ & $=$ & $\langle x, f^* \circ f\left(y\right) \rangle$ par caractérisation de l'adjoint \\
& $=$ & $\langle x, f^{-1} \circ f\left(y\right) \rangle$ par hypothèse \\
& $=$ & $\langle x, y\rangle$. \\
\end{tabular}

\vspace*{.1cm}

Donc $f$ est orthogonal. 
\end{itemize}
\end{proof}

\vspace*{-.3cm}

\begin{theoreme} (voir \cite{Keller_notes})
Soit $f \in \mathrm{GL}\left(E\right)$. Alors il existe une unique décomposition $$f = f_of_+$$ où $f_o$ est orthogonale et $f_+$ est symétrique et définie positive ; on l'appelle la décomposition polaire. \\

En outre, si $g \in \mathrm{GL}\left(E\right)$ commute avec $f^*f$, alors $g$ commute avec $f_+$. 
\end{theoreme}

\begin{proof}
\begin{itemize}
\item[(Existence)] On considère $f^*f$. On a : $$\left(f^*f\right)^* = f^*f^{**} = f^*f.$$

Donc d'après la propriété \ref{auto_adjoint_symetrie}, $f^*f$ est symétrique. De plus, on a $$\forall v \in E \setminus \lb 0 \rb, \langle v, f^*f\left(v\right) \rangle = \langle f\left(v\right), f\left(v\right) \rangle > 0$$

Donc par définition, $f^*f$ est défini positif. \\

Or, d'après la propriété \ref{valeurs_propres_positives}, $f^*f$ est diagonalisable avec des valeurs propres strictement positives. Soit $r \in \N^*$. Notons $\lambda_1, \lambda_2,\ldots, \lambda_r$ les valeurs propres (deux à deux distinctes) de $f^*f$. On note $E_{\lambda_i}$ l'espace propre de la valeur propre $\lambda_i$, pour $i \in \llbracket 1, r\rrbracket$ ; alors $E = \sommeOrthogonale{i = 1}{r}{E_{\lambda_i}}$. \\

Soit $i \in \llbracket 1, r\rrbracket$. On définie $f_+ = \sqrt{f^*f}$ par $\forall v \in E_{\lambda_i}, f_+(v) = \sqrt{\lambda_i} \cdot v$. Puisque $f$ est symétrique, il est clair que $f_+$ est symétrique. Ainsi, d'après \ref{f_sym_f_moins_1_sym}, $f_+^{-1}$ est symétrique. \\

Puisqu'on veut $f = f_of_+$, on définie $f_o = ff_+^{-1}$. Montrons que $f_o$ est orthogonale. \\

\begin{tabular}{lll}
\hspace*{-.35cm} On a $f_o^*f_o$ & $=$ & $\left(ff_+^{-1}\right)^*ff_+^{-1}$ \\
& $=$ & $f_+^{-*}f^{*}ff_+^{-1}$ \\
& $=$ & $f_+^{-*}\left(f^{*}f\right)f_+^{-1}$ par associativité de la composition \\
\end{tabular}

\vspace*{.3cm}

Or, $f^*f$ est $f_+$ sont diagonales dans une même base. Donc ces deux applications commutent. Ainsi : \\

\begin{tabular}{lll}
\hspace*{-.35cm} $f_o^*f_o$ & $=$ & $f_+^{-*}f_+^{-1}f^*f$ \\
& $=$ & $f_+^{-1} f_+^{-1} f^* f$ car $f_+^{-1}$ est symétrique \\ 
& $=$ & $\left(f_+^2 \right)^{-1} f^*f$ \\
& $=$ & $\left(f^*f\right)^{-1}\left(f^*f\right)$ par définition de $f_+$ \\
& $=$ & $\mathrm{Id}_E$ \\
\end{tabular}

\vspace*{.3cm}

Donc d'après la propriété \ref{inverse_orthogonal}, on a $f_o$ est orthogonal. \\

Supposons maintenant que $g \in \mathrm{GL}\left(E\right)$ commute avec $f^*f$ et montrons que $g$ commute avec $f_+$. \\

Soit $i \in \llbracket 1, r\rrbracket$. Alors, si $v \in E_{\lambda_i}$, on a : \\ 

\begin{tabular}{lll}
\hspace*{-.35cm} $\left(f^*f\right)g\left(v\right)$ & $=$ & $g\left(f^*f\left(v\right)\right)$ car $g$ commute avec $f^*f$ \\
& $=$ & $g\left(\lambda_i v\right)$ car $v$ est vecteur propre pour la valeur propre $\lambda_i$ \\
& $=$ & $\lambda_i g\left(v\right)$ par linéarité. \\
\end{tabular}

\vspace*{.3cm}

Donc $\left(f^*f\right)g\left(v\right) = \lambda_i g\left(v\right)$. Ainsi $g\left(E_{\lambda_i}\right) \subseteq E_{\lambda_i}$. 

Pour montrer que $f_+ \circ g = g \circ f_+$ sur $E$, comme $E = \sommeOrthogonale{i = 1}{r}{E_{\lambda_i}}$, il suffit de montrer que $\forall i \in \llbracket 1, r\rrbracket, \forall v \in E_{\lambda_i}, f_+ \circ g\left(v\right) = g \circ f_+\left(v\right)$. \\
 
Soit $i \in \llbracket 1, r\rrbracket$ et soit $v \in E_{\lambda_i}$. On a $f_+ \circ g\left(v\right) = \sqrt{\lambda_i} \cdot g\left(v\right)$ car $g\left(v\right) \in E_{\lambda_i}$ et $g \circ f_+\left(v\right) = g\left(\sqrt{\lambda_i} \cdot v\right)$ car $v \in E_{\lambda_i}$. On en conclut l'égalité $f_+ \circ g\left(v\right) = g \circ f_+\left(v\right)$ par linéarité de $g$. \\

\item[(Unicité)] Supposons que $f = f_of_+ = f_o'f_+'$ pour un $f_o'$ orthogonal et un $f_+'$ symétrique et défini positif. \\ 

\begin{tabular}{lll}
$f^*f$ & $=$ & $\left(f_o' f_+'\right)^* f_o' f_+'$ \\
& $=$ & $f_+'^* f_o'^* f_o' f_+'$ \\
& $=$ & $f_+'^* f_+'$ car $f \in \mathcal{O}\left(E\right)$ \\
& $=$ & $f_+' f_+'$ car $f'_+$ est symétrique \\
& $=$ & $f_+'^2$ \\
\end{tabular}

Soit $s \in \N^*$. $f_+'$ est diagonalisable de valeurs propres $\mu_1, \mu_2, \ldots, \mu_s$, donc $f_+'^2$ est diagonalisable de valeurs propres $\mu^2_1, \mu^2_2, \ldots, \mu^2_s$. \\

Or, $f_+'^2 = f^*f$, donc $\mu_1^2, \mu_2^2, \ldots, \mu^2_s$ sont exactement les valeurs propres de $f^*f$, i.e. $\lambda_1, \lambda_2, \ldots, \lambda_r$. \\
Ainsi $\mu_1, \mu_2, \ldots, \mu_s$ sont les $\sqrt{\lambda_1}, \sqrt{\lambda_2}, \ldots, \sqrt{\lambda_r}$. \\

On a $E_{\mu_i}\left(f_+'\right) = E_{\mu^2}\left(f_+'^2\right) = E_{\lambda_j}\left(f^*f\right)$. Donc $f_+'$ a les mêmes valeurs propres avec les mêmes espaces propres que $f_+$. D'où l'unicité de la décomposition polaire.
\end{itemize}
\end{proof}

\begin{corollaire}
Si $\phi, \psi \in \mathcal{O}\left(E\right)$ et $g \in \mathrm{GL}\left(E\right)$ tel que $g \varphi g^{-1} = \psi$, alors $g_0 \varphi g_0^{-1} = \psi$, où $g = g_og_+$ est la décomposition polaire.
\end{corollaire}



\begin{figure}[h]
\begin{center}
%%%% D4 
%%%% Supprimer les cercles qui entourent e^{i\phi} et \gamma
%%%% Supprimer la flèche et \gamma, et écrire directement $\abs{z} = \sqrt{z\overline{z}}$
%%%% Aligner le petit point (qui marque le cercle du cercle) à l'intersection des deux droites (là, il est un peu décalé). 
%%%% Réduire la taille de la police (tiny ?)
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=1]
% \clip(-2.25,-2.38) rectangle (2.37,2.39);
\draw(-0,0) circle (2cm);


\draw (-3,0.05)-- (7,0.05);
\draw (0.07,-3)-- (0.07,3);
%\draw (3.5,-2)-- (3.5,1);

\draw [o-open triangle 60] (0,0) -- (30:4) ; 
\draw  [o-open triangle 60] (0,0) -- (30:2) ; 


% \draw (45:2.5) ellipse (0.5cm and 0.35cm);
\draw(45:3) node [below left] {$e^{i\theta}$};

% \draw(3.5,1.25)  circle (0.35cm) ; 
\draw (3,1) node {$\abs{z} = \sqrt{z\overline{z}}$} ;
\draw (4,2.25) node {$\abs{z}e^{i\theta}$} ;

\end{tikzpicture}
\end{center}
\caption{Représentation polaire d'un nombre complexe}
\end{figure}

%\newpage

\section{Théorie des groupes}

\begin{definition}
Soit $\left(G, \top\right)$ un magma. On dit que $\left(G, \top\right)$ est un \textit{groupe} si : 

\begin{itemize}
\item[•] La loi $\top$ est associative ;
\item[•] $G$ admet un élément neutre $e$ pour la loi $\top$ ;
\item[•] Tout élément de $G$ admet un symétrique.
\end{itemize}

\vspace*{.3cm}

Si $\top$ est commutative, on parle de groupe commutatif ou abélien. \\
Si $G$ est fini, on parle de groupe fini. 
\end{definition}

\begin{definition} Soit $G$ un groupe fini. Le cardinal de $G$ est appelé \textit{ordre du groupe} $G$.
\end{definition}

\begin{definition}
Soient $\left(G,\top\right)$ et $\left(G', \top'\right)$ deux groupes. Soit $f \in G \longrightarrow G'$. On dit que $f$ est un \textit{morphisme} du groupe $\left(G, \top\right)$ dans le groupe $\left(G', \top'\right)$ si : $$\forall \left(x,y\right) \in G^2, f\left(x \top y\right) = f\left(x\right) \top' f\left(y\right).$$
\end{definition}

\begin{definition} Soit $f$ un morphisme d'un groupe $\left(G,\top \right)$ dans un groupe $\left(G', \top'\right)$. On appelle \textit{noyau} de $f$ l'ensemble : $\Ker f = \enstq{x \in G}{f(x) = e'}$, où $e'$ est l'élément neutre de $\left(G', \top'\right)$.
\end{definition}

\begin{definition}
Soient $G$ et $G'$ deux groupes. Soit $f \in G \longrightarrow G'$.  Si $f$ est bijective, on dit que $f$ est un \textit{isomorphisme} de groupes. 
\end{definition}

\begin{definition}
Soient $G$ et $G'$ deux groupes. On dit que $G$ et $G'$ sont \textit{isomorphes} s'il existe un isomorphisme de $G$ dans $G'$. 
\end{definition}

\begin{definition}
Soit $\left(G, \top\right)$ un groupe. Soit $H$ une partie de $G$. \\ On dit que $H$ est un \textit{sous-groupe} de $\left(G, \top\right)$ si : 

\begin{itemize}
\item[•] $H$ est stable pour la loi $\top$  ;
\item[•] $\left(H, \top\right)$ est un groupe.
\end{itemize}
\end{definition}

\begin{proposition} \label{caracterisation_sous_groupes}
Soit $\left(G, \cdot\right)$ un groupe. 	Soit $H$ une partie de $G$. Alors les propositions suivantes sont équivalentes : \\

\begin{itemize}
\item[$\left(i\right)$] $H$ est un sous-groupe de $G$ \\
\item[$\left(ii\right)$] $\left\{
\begin{array}{l}
H \neq \emptyset \\
\forall \left(x,y\right) \in H^2, x \cdot y \in H \\
\forall x \in H, x^{-1} \in H \\
\end{array}
\right.$ 
\end{itemize}
\end{proposition}

\begin{proposition} \label{f_mois_1_sous-groupe}
Soient $\left(G, \top\right)$ et $\left(G', \top'\right)$ deux groupes. Soit $f$ un morphisme de $\left(G, \top\right)$ dans $\left(G', \top'\right)$. Soit $H'$ un sous-groupe de $\left(G', \top'\right)$. Alors $f^{-1}\left(H'\right)$ est un sous-groupe de $\left(G, \top\right)$.
\end{proposition}

\begin{proof} Montrons par le point $\left(ii\right)$ de la proposition \ref{caracterisation_sous_groupes} que $f^{-1}\left(H'\right)$ est un sous-groupe de $G$. \\

Il est clair que $f^{-1}\left(H'\right) \subseteq H$. 
Notons $e$ et $e'$ les éléments neutres respectifs de $\left(G, \top\right)$ et $\left(G', \top'\right)$. $f$ est un morphisme donc $f(e) = f\left(e \top e\right) = f(e) \top' f(e)$, donc par régularité, $f(e) = e'$. 
Or, $e' \in H'$. Donc $f(e) \in H'$, ou encore $e \in f^{-1}\left(H'\right)$.
Ainsi $f^{-1}\left(H'\right) \neq \emptyset$. \\

Soit $\left(x,y\right) \in \left(f^{-1}\left(H'\right)\right)^2$. On a donc $f(x) \in H'$ et $f(y) \in H'$. 
Ainsi $f(x) \top' f(y) \in H'$ car $H'$ est un sous-groupe de $\left(G', \top'\right)$. 
Or, $f(x) \top' f(y) = f\left(x \top y\right)$ car $f$ est un morphisme. \\ Donc $f\left(x \top y\right) \in H'$, i.e. $x \top y \in f^{-1}\left(H'\right)$. \\

Enfin, soit $x \in f^{-1}\left(H'\right)$. Alors $f(x) \in H'$, donc si on note $\overset{-1}{\top} A$ l'inverse de $A$ pour $\top$, \\ on a $\overset{-1}{\top} f\left(x\right) \in H'$, car $H'$ est un sous-groupe de $\left(G', \top'\right)$. 
Ainsi $\overset{-1}{\top} f\left(x\right) \in H'$, i.e. $f\left(\overset{-1}{\top} x\right) \in H'$ car $f$ est un morphisme de groupes. Donc $\overset{-1}{\top} x \in f^{-1}\left(H'\right)$. \\

On en conclut que $f^{-1}\left(H'\right)$ est bien un sous-groupe de $\left(G, \top\right)$.
\end{proof}

\begin{definition}
Soit $\left(G,\top \right)$ un groupe. Soit $H$ un sous-groupe de $G$. \\
On dit que $H$ est un \textit{sous-groupe distingué} dans $G$ si : $$\forall g \in G, gHg^{-1} \subseteq H.$$
\end{definition}

\begin{proposition} \label{f_mois_1_sous-groupe_distingue}
Soient $\left(G, \top\right)$ et $\left(G', \top'\right)$ deux groupes. Soit $f$ un morphisme de $\left(G, \top\right)$ dans $\left(G', \top'\right)$. Soit $H'$ un sous-groupe distingué de $\left(G', \top'\right)$. \\ Alors $f^{-1}\left(H'\right)$ est un sous-groupe distingué de $\left(G, \top\right)$.
\end{proposition}

\begin{proof}
D'après la proposition \ref{f_mois_1_sous-groupe}, $f^{-1}\left(H'\right)$ est un sous-groupe de $\left(G, \top\right)$. \\
Soient $g \in G$ et $h \in f^{-1}\left(H'\right)$. 
Alors $f\left(ghg^{-1}\right) = f\left(g\right)f\left(h\right)f\left(g^{-1}\right)$, car $f$ est un morphisme. 
On a naturellement $f\left(g\right) \in H'$ et $f\left(\overset{-1}{\top} g\right) \in H'$. De plus, puisque $H'$ est distingué dans $G'$, on a $f\left(h\right) \in H'$. 
Donc $f\left(ghg^{-1}\right) = f\left(g\right)f\left(h\right)f\left(g^{-1}\right) \in H'$, i.e $ghg^{-1} \in f^{-1}\left(H'\right)$. 
On en conclut que $f^{-1}\left(H'\right)$ est distingué dans $G$.
\end{proof}

\begin{theoreme} \label{noyau_distingue}
Soit $f$ un morphisme d'un groupe $\left(G,\top \right)$ dans un groupe $\left(G', \top'\right)$. \\
Le noyau de $f$ est un sous-groupe distingué du groupe $G$.
\end{theoreme}

\begin{proof} On a $\Ker\left(f\right) = f^{-1}\left(\lb e\rb\right)$. \\
Il est clair que $\lb e \rb$ est un sous-groupe distingué. 
Donc, d'après la propriété \ref{f_mois_1_sous-groupe_distingue}, $f^{-1}\left(\lb e\rb\right)$ est un sous-groupe distingué de $G$. 
On en conclut que $\Ker\left(f\right)$ est un sous-groupe distingué de $G$.
\end{proof}

\chapter{Arbre de décision}

%\pagestyle{empty}
\centerline{
% \rotatebox{90}
{
\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=.8]
%\node (Z) at (20,15) {\rotatebox{-90}{$\mathbf{\mathrm{Cas n=10}}}$} ;
\node (YES) at (7.15,14.25) {oui} ;
\node (NO) at (8.95,14.2) {non} ;
\draw [dotted](-8.6,1)-- (-8.6,16);
\draw [dotted](8,1)-- (8,13.5);
\node (o1) at (-10.5,15.5) {Plus petit angle} ;
\node (o2) at (-11,15) { de rotation} ;
\draw [dotted](-12,14)-- (11,14);
\node (o3) at (-10.5,13) {$360^\circ/6 = 60^\circ$} ;
\node (o4) at (-10.5,11) {$360^\circ/4 = 90^\circ$} ;
\node (o5) at (-10.5,8.5) {$360^\circ/3 = 120^\circ$} ;
\node (o5) at (-10.5,5) {$360^\circ/2 = 180^\circ$} ;
\node (o5) at (-10.5,2.5) {aucun $360^\circ$} ;
\node (A) at (8,15) {Contient une réflexion} ;
\node (B) at (7,13) {$p6m$} ;
\node (C) at (9,13) {$p6$} ;
\draw [dotted](-12,12.5)-- (11,12.5);
\node (D) at (6,12) {Miroir à $45^{\circ}$} ;
\node (E) at (4,10) {$p4m$} ;
\node (F) at (6,10) {$p4g$} ;
\node (G) at (9,10) {$p4$} ;
\draw [dotted](-12,9.5)-- (11,9.5);
\node (H) at (2,9) {Centre de rotation à l'extérieur des miroirs} ;
\node (I) at (1,8) {$p31m$} ;
\node (J) at (3,8) {$p3m1$} ;
\node (K) at (9,8) {$p3$} ;
\draw [dotted](-12,7.5)-- (11,7.5);
\node (L) at (2,7) {Axe des symétries perpendiculaires} ;
\node (M) at (-2,5) {Centres de rotation en dehors des miroirs} ;
\node (N) at (3,5.75) {$pmg$} ;
\node (O) at (-3,4) {$cmm$} ;
\node (P) at (-1,4) {$pmm$} ;
\node (Q) at (10.25,7) {Symétries glissées} ;
\node (R) at (8.75,4) {$pgg$} ;
\node (S) at (10.75,4) {$p2$} ;
\draw [dotted](-12,3.5)-- (11,3.5);
\node (T) at (-3,3) {Axes de symétries glissées en dehors des miroirs} ;
\node (U) at (-4,1.75) {$cm$} ;
\node (V) at (-2,1.75) {$pm$} ;
\node (W) at ((10.25,3) {Symétries glissées} ;
\node (X) at (8.75,1.75) {$pg$} ;
\node (Y) at (10.75,1.75) {$p1$} ;
%
\draw[->, >=latex](A) to (B) ;
\draw[->, >=latex](A) to (C) ;
%\draw[->, >=latex](B) to (D) ;
\draw[->, >=latex](D) to (E) ;
\draw[->, >=latex](D) to (F) ;
%\draw[->, >=latex](E) to (H) ;
\draw[->, >=latex](H) to (I) ;
\draw[->, >=latex](H) to (J) ;
\draw[->, >=latex](L) to (M) ;
\draw[->, >=latex](L) to (N) ;
\draw[->, >=latex](M) to (O) ;
\draw[->, >=latex](M) to (P) ;
\draw[->, >=latex](T) to (U) ;
\draw[->, >=latex](T) to (V) ;
\draw[->, >=latex](Q) to (R) ;
\draw[->, >=latex](Q) to (S) ;
\draw[->, >=latex](W) to (X) ;
\draw[->, >=latex](W) to (Y) ;
\end{tikzpicture}
}
}


\ifdefined\SUBCOMPLETE
\else
    \end{document}
\fi
