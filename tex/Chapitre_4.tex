\ifdefined\COMPLETE
\else
    \input{./preambule-sacha-utf8.ltx}   
    \usepackage{variations}
    \begin{document}
\fi

\setcounter{theoreme}{0}

\chapter{Cohomologie des groupes}

\section{Cohomologie en degré $1$}

\subsection{Définitions}

Soit $G$ un groupe. Soit $D$ un groupe abélien muni d'une action de $G$ par automorphismes. \\

\begin{definition}
Un \textit{$1$-cocycle} sur $G$ à valeurs dans $D$ est une application $s : G \longrightarrow D$ telle que $$\forall \left(\alpha, \beta\right) \in G^2, s\left(\alpha \beta\right) = s\left(\alpha\right) + \alpha s\left(\beta\right).$$
\end{definition}

\begin{definition}
Soit $e \in D$. Le \textit{$1$-cobord} $s_e$ associé à $e$ est tel que : $$ \forall \alpha \in G, s_e\left(\alpha\right) = e - \alpha e.$$
\end{definition}

\begin{lemme} (voir \cite{Keller_notes})
Tout $1$-cobord est un $1$-cocycle. 
\end{lemme}

\begin{proof}
Soit $e \in D$ et $s_e$ le $1$-cobord associé à $e$.

On a d'une part $s_e\left(\alpha \beta\right) = e - \alpha \beta e$. \\
D'autre part : $s_e\left(\alpha\right) + \alpha s_e\left(\beta\right) = e - \alpha e + \alpha\left(e - \beta e\right) = e - \alpha e + \alpha e - \alpha \beta e = e - \alpha \beta e$. \\
D'où $s_e\left(\alpha \beta\right) = s_e\left(\alpha\right) + \alpha s_e\left(\beta\right)$, et ceci pour tout $\left(\alpha, \beta\right) \in G^2$. \\
Donc $s_e$ est un $1$-cocycle.
\end{proof}

\begin{definition}
Le \textit{premier groupe de cohomologie} $H^1\left(G, D\right)$ est tel que : $$H^1\left(G, D\right) = \lb 1\mathrm{-cocycles}\rb / \lb 1\mathrm{-cobords}\rb.$$
\end{definition}

\newpage

\subsection{Interprétation}

\subsubsection{$H^{1}\left(\Gamma, L\right)$ et compléments} 

On considère le produit semi-direct $$G = L \rtimes \Gamma \cong t_L \cdot s_0\left(\Gamma\right) \subseteq \mathrm{Isom}\left(\mathcal{E}\right)$$ pour un choix $O \in \mathcal{E}$.

\begin{definition}
Tout sous-groupe $\Gamma' \subset L \rtimes \Gamma$ tel que l'application $g \in \Gamma' \longmapsto \overrightarrow{g} \in \Gamma$ est un isomorphisme est appelé \textit{complément} du réseau $L$ dans $G$. 
\end{definition}

\begin{proposition} On a une bijection entre l'ensemble  des classes de translation par $L$ de complément de $L$ dans $G$ et $H^{1}\left(\Gamma, L\right)$. 

\begin{proof} ~ \\
\begin{itemize}
\item[•] On considère $\Gamma'$ tel que $\Gamma' \overset{\sim}{\longrightarrow} \Gamma$. Pour $g \in \Gamma$, on note $s\left(g\right) \in \Gamma'$ pour l'unique élément $s\left(g\right) \in \Gamma'$ tel que $\overrightarrow{s\left(g\right)} = g$. On a donc $$s\left(g\right) = t_{c\left(g\right)} \cdot s_O\left(g\right)$$ pour un unique $c\left(G\right) \in L$. \\

On a alors, d'une part : $s\left(g_1g_2\right) = t_{c\left(g_1g_2\right)} s_O\left(g_1g_2\right)$ par définition, mais aussi :

\begin{tabular}{llll}
\hspace*{-.35cm} $s\left(g_1g_2\right)$ & $=$ & $s\left(g_1\right) s\left(g_2\right)$ \\
& $=$ & $t_{c\left(g_1\right)}s_O\left(g_1\right) t_{c\left(g_2\right)}s_O\left(g_2\right)$ \\
& $=$ & $t_{c\left(g_1\right)}s_O\left(g_1\right) t_{c\left(g_2\right)} \left(s_O\left(g_1\right)^{-1} s_O\left(g_1\right)\right) s_O\left(g_2\right)$ \\
& $=$ & $t_{c\left(g_1\right)}\left(s_O\left(g_1\right) t_{c\left(g_2\right)}s_O\left(g_1\right)^{-1}\right) s_O\left(g_1\right) s_O\left(g_2\right)$ \\
& $=$ & $t_{c\left(g_1\right)} t_{g_1 c\left(g_1\right)} s_O\left(g_1\right) s_O\left(g_2\right)$ & car $f \circ t_v \circ f^{-1} = t_{\overrightarrow{f}\left(v\right)}$ \\
& $=$ & $t_{c\left(g_1\right) + g_1c\left(g_2\right)} s_O\left(g_1g_2\right)$ \\ 
\end{tabular}

\vspace*{.3cm}

Donc $c\left(g_1g_2\right) = c\left(g_1\right) + g_1c\left(g_2\right)$, et l'application $g \longmapsto c\left(g\right)$ est un cocycle $\Gamma \longrightarrow L$. \\

\item[•] On peut montrer que la classe de $c : \Gamma \longrightarrow L$ ne dépend que de la classe de translation par $L$ de $\Gamma'$. \\

\item[•] On considère maintenant l'application réciproque, en posant $$\Gamma'_c = \enstq{t_{c\left(g\right)} \cdot s_O\left(g\right)}{g \in \Gamma} \overset{\sim}{\longrightarrow} \Gamma.$$

On peut montrer que la classe de translation de $\Gamma'_c$ ne dépend que de la classe de cohomologie de $c$. \\
\end{itemize}
\end{proof}

\end{proposition}

\subsubsection{$H^1\left(\Gamma, E/L\right)$ et classes de translation de groupes cristallographiques}

\begin{lemme}\label{cohomologie_annule} (voir \cite{Iversen})
Si $G$ est un groupe fini d'ordre $N$ et $N \cdot : D \longrightarrow D$, $x \longmapsto N \cdot x$ une application bijective, alors $H^{1}\left(G, D\right) = \lb 0 \rb$.\end{lemme}

\begin{proof} 
Soit $s$ un cocycle de $G$ dans $D$. Alors : $$\somme{g \in G}{}{s\left(g\right)} = \somme{g \in G}{}{s\left(a g\right)} = \somme{g \in G}{}{s\left(a\right)} + a s\left(g\right) = Ns\left(a\right) + \somme{g \in G}{}{s\left(g\right)}$$ où $a \in G$. Si on note $k = \somme{g \in G}{}{s\left(g\right)}$, on a l'égalité $Ns\left(a\right) = k - a k$. \\

Si $N\cdot$ est bijective et que l'on note $N^{-1} \cdot$ l'application réciproque de $N \cdot$, il vient $$s(a) = \left(N^{-1}k\right) - a(N^{-1}k),$$ et ainsi $s$ est un cobord. On en conclut que tout $1$-cocycle est aussi un $1$-cobord, et \\ ainsi $H^{1}\left(G, D\right) = \lb 0 \rb$.
\end{proof}

$\Gamma$ agit sur $E$ en laissant stable $L$, où $\Gamma \subseteq O\left(E\right)$. On peut donc s'intéresser à trois groupes de cohomologie de degré $1$ : $H^{1}\left(\Gamma, L\right)$, $H^{1}\left(\Gamma, E\right)$ et $H^{1}\left(\Gamma, E/L\right)$. \\

Or, on a $H^{1}\left(\Gamma, E\right) = \lb 0 \rb$ par le lemme \ref{cohomologie_annule}. On verra à la section \ref{lien_cohom_cristal} qu'on a une bijection entre $H^1(\Gamma,E/L)$ et l'ensemble des classes de translation de groupes cristallographiques $G \subset \mathrm{Isom}\left(\mathcal{E}\right)$ de réseau $L$ et de groupe ponctuel $\Gamma$.

\subsection{Propositions}

\begin{proposition}(voir \cite{Iversen})
Soit $G \in \mathrm{Isom}\left(\mathcal{E}\right)$ un groupe cristallographique. Soit $\mathcal{T} \subseteq G$ le sous-groupe des translations. Alors $\mathcal{T}$ est le plus grand sous-groupe abélien distingué dans $G$. 
\end{proposition}

\begin{remarque}
Si $G_1$, $G_2$ sont deux groupes cristallographiques de groupes des translations respectivement $\mathcal{T}_1$ et $\mathcal{T}_2$, et si $\varphi : G_1 \longrightarrow G_2$ est un isomorphisme de groupes, alors $\varphi\left(\mathcal{T}_1\right) = \mathcal{T}_2$.
\end{remarque}

\begin{theoreme} \textbf{(Bieberbach)} (voir \cite{Iversen}) \label{Bieberbach} Soient $G_1$ et $G_2$ deux groupes cristallographiques. Si $\varphi : G_1 \longrightarrow G_2$ est un isomorphisme, alors il existe $g \in \mathrm{GA}\left(\mathcal{E}\right)$ tel que $$\forall x \in G_1, \varphi\left(x\right) = gxg^{-1}.$$
\end{theoreme}

\subsection{Lien entre cohomologie et groupes cristallographiques}\label{lien_cohom_cristal}

Soient $\mathcal{E}$ un plan affine euclidien et $L$ un réseau de $E$. Dans toute cette partie, on notera $\pi : E \longrightarrow E/L$ la projection canonique. Soit $\Gamma \subseteq \mathcal{O}\left(E\right)$ un sous-groupe stabilisant $L$.

On choisit une origine $O \in \mathcal{E}$. Alors on a un homomorphisme $s_O : \mathcal{O}\left(E\right) \longrightarrow \mathrm{Isom}\left(\mathcal{E}\right)$ tel que $\overrightarrow{\cdot} \circ s_O = \mathrm{Id}$ défini par \vspace*{-.3cm} $$s_O\left(g\right)\left(P\right) = O + g\left(\overrightarrow{OP}\right).$$ où $g \in \mathcal{O}\left(E\right)$ et $P \in \mathcal{E}$. 

\vspace*{-50cm}

\newpage

Pour un $1$-cocycle $c : \Gamma \longrightarrow E/L$, on définit la partie $G_c$ de $\mathrm{Isom}\left(\mathcal{E}\right)$ par : $$G_c = \enstq{t_vs_O\left(g\right)}{\pi\left(v\right) = c\left(g\right), v \in E, g \in G}.$$  

\begin{lemme}(voir \cite{Keller_notes}) \label{lemme_lien}
\begin{itemize}
\item[a)] $G_c$ est un sous-groupe de $\mathrm{Isom}\left(\mathcal{E}\right)$. Le sous-groupe des translations de $G_c$ est $L$ et le groupe ponctuel de $G_c$ est $\Gamma$. 
\item[b)] Soit $w \in E$ et $b$ le cobord associé à $\pi\left(w\right)$, i.e. $b\left(g\right) = \pi\left(w\right) - g\pi\left(w\right)$ pour tout $g \in \Gamma$. Alors on a $$G_{c+b} = t_w G_c t_w^{-1}.$$
\item[c)] Réciproquement, supposons que pour un un cocycle $c'$ il existe $w \in E$ tel que $$G_{c'} = t_w G_c t_w^{-1}.$$ Alors on a $c' = c + b$, où $b$ est le cobord associé à $\pi\left(w\right)$. 
\item[d)] Soit $G \subset \mathrm{Isom}\left(\mathcal{E}\right)$ un groupe cristallographique dont le réseau des translations est $L$ et dont le groupe ponctuel est $\Gamma$. Alors il existe un cocycle $c$ tel que $G = G_c$. 
\end{itemize}
\end{lemme}

Les points a), b) et c) montrent que l'application $c \longmapsto G_c$ induit une application bien définie et injective de $H^{1}\left(\Gamma, E/L\right)$ vers l'ensemble des classes de translation par $E$ de sous-groupes cristallographiques $G$ de $\mathrm{Isom}\left(\mathcal{E}\right)$ dont le sous-groupe des translations est $L$ et le groupe ponctuel $\Gamma$. De plus, le point d) montre que cette application est surjective. D'où le résultat suivant : 

\begin{corollaire}
L'application $c \longmapsto G_c$ induit une bijection de $H^{1}\left(\Gamma, E/L\right)$ sur l'ensemble des groupes cristallographiques $G \subset \mathrm{Isom}\left(\mathcal{E}\right)$ de réseau $L$ et de groupe ponctuel $\Gamma$.
\end{corollaire}

\textit{Démonstration du lemme \ref{lemme_lien}}. \\

\begin{itemize}
\item[a)] La condition de cocycle implique que $G_c$ est stable par multiplication. La condition de cocycle implique également que $c\left(g^{-1}\right) = -g^{-1} c\left(g\right)$ pour tout $g \in \Gamma$. Cette égalité montre donc que $G_c$ est stable par passage à l'inverse. Donc $G_c$ est bien un sous-groupe. Montrons maintenant que le sous-groupe des translations de $G_c$ est égal à $L$. : \\

\begin{itemize}
\item[$\left(\subseteq\right)$] La condition de cocycle donne $$c\left(e\right) = c\left(ee\right) = c\left(e\right) + ec\left(e\right) = c\left(e\right) + c\left(e\right)$$ et donc $c\left(e\right) = 0$. Pour $v \in L$, on a $$t_v = t_v s_O\left(e\right)$$ et cet élément appartient à $G_c$ car $\pi\left(v\right) = 0 = c\left(e\right)$. D'où l'inclusion $L \subseteq G_c$. \\

\item[$\left(\supseteq\right)$] Si on a $g \in \Gamma$ et $v \in E$ tel que $\pi\left(v\right) = c\left(g\right)$ et que $\overrightarrow{t_v s_O\left(g\right)} = \mathrm{Id}$, alors $g = \overrightarrow{s_O\left(g\right)} = \overrightarrow{t_v s_O\left(g\right)} = \mathrm{Id}$ et donc $\pi\left(v\right) = c\left(e\right) = 0$ ce qui signifie que $v \in L$. \\
\end{itemize}
Le sous-groupe des translations de $G_c$ est donc bien $L$.

\newpage

Enfin, comme on a $$\overrightarrow{t_v s_O\left(g\right)} = \overrightarrow{s_O\left(g\right)} = g$$ pour $g \in \Gamma$, il est clair que le groupe ponctuel de $G_c$ est $\Gamma$. \\

\item[b)] Montrons que $t_w G_c t_w^{-1} = G_{c + b}$. \\

\begin{itemize}
\item[$\left(\subseteq\right)$] Par définition, les éléments de $t_w G_c t_w^{-1}$ sont de la forme $t_w t_v s_O\left(g\right) t_w^{-1}$, où $g \in \Gamma$ et $\pi\left(v\right) = c\left(g\right)$. Notons $\gamma = s_O\left(g\right)$. On a : $$t_w t_v \gamma t_w^{-1} = t_w t_v\gamma t_{-w} \gamma^{-1} \gamma
= t_{v+w} t_{-gw} \gamma = t_{v+w - gw} \gamma $$ et $\pi(v+w-gw) = \pi(v) + \pi(w) - g\pi(w) = c(g) + b(g)$. D'où $t_w G_c t_w^{-1} \subseteq G_{c+b}$. \\

\item[$\left(\supseteq\right)$] Réciproquement, les éléments de $G_{c + b}$ sont de la forme $t_{v'}s_O\left(g\right)$, où $g \in \Gamma$ et $$\pi(v')=c(g)+b(g)=c(g) +\pi(w)- g \pi(w).$$ Posons $v=v'-w-gw$. Alors on a $$
t_{v'} s_O(g) = t_{v+w-gw} \gamma = t_v t_w t_{-gw} \gamma =
t_v t_w \gamma t_{-w} \gamma^{-1} \gamma = t_w t_v \gamma t_w^{-1},$$ ce qui montre que $G_{c+b}\subseteq t_w G_c t_w^{-1}$.
\end{itemize}

\item[c)] Soit $g\in \Gamma$ et $\gamma = s_O(g)$. Soit $v\in E$ tel que $\pi(v)=c(g)$.
On a déjà vu que $$t_w t_v \gamma t_w^{-1} = t_{v+w-gw} \gamma.$$
Par hypothèse, cet élément appartient à $G_{c'}$ et on a donc
$$c'(g) = \pi(v+w-gw) = \pi(v)+\pi(w)-g\pi(w) = c(g)+b(g).$$
Cela montre qu'on a bien $c'=c+b$. \\

\item[d)] Montrons que $G = G_c$. \\

\begin{itemize}
\item[$\left(\subseteq\right)$] Soit $\gamma \in G$ et $g=\overrightarrow{\gamma}$. On a $\overrightarrow{\gamma} = g = \overrightarrow{s_O(g)}$ et il existe donc une translation $t_v$ telle que $\gamma = t_v s_O(g)$.
On pose $c(g) = \pi(v)$. Pour deux éléments $\gamma_1$ et $\gamma_2$
de parties linéaires $g_1$ et $g_2$, il existe des $v_i\in E$ tels que
$\gamma_i=t_{v_i} s_O(g_i)$, $i=1,2$. Pour $\gamma=\gamma_1\gamma_2$
de partie linéaire $g=g_1 g_2$, on a $v\in E$ tel que $\gamma=t_v s_O(g)$. \\
On a $$\gamma_1 \gamma_2 = t_{v_1} s_O(g_1) t_{v_2} s_O(g_2)
=t_{v_1} t_{g_1 v_2} s_O(g_1 g_2) = t_{v_1 + g_1 v_2} s_O(g) = t_v s_O(g).$$
Donc $v= v_1 + g_1 v_2$ et $$
c(g_1 g_2) = \pi(v) = \pi(v_1) + \pi(g_1 v_2) = c(g_1) + g_1 \pi(v_2) = c(g_1) + g_1 c(g_2)$$
ce qui est la condition de cocycle. Par construction, on a $G \subseteq G_c$. \\

\item[$\left(\supseteq\right)$] Réciproquement, supposons que l'on a un élément $t_v s_O(g)$ de $G_c$, c'est-à-dire que $\pi(v)=c(g)$. Alors il existe un élément $\gamma\in G$ tel
que $\overrightarrow{\gamma}=g$ et on a $\gamma=t_{v'} s_O(g)$ pour un $v'\in E$.
Par définition, on a $\pi(v')=c(g)$. Or on a $c(g)=\pi(v)$. Donc $v=v'+l$ pour un élément $l\in L$. On trouve que $$t_v s_O(g)= t_{v'+l} s_O(g) = t_l t_{v'} s_O(g) = t_l \gamma.$$
Donc l'élément $t_v s_O(g)$ appartient bien à $G$ et on a $G_c \subseteq G$.
\end{itemize}



\end{itemize}


\qed










\iffalse


En général, $s_O\left(\overrightarrow{G}\right) \not\subseteq G$. Cependant, pour chaque $g \in \overrightarrow{G}$, il existe un $t_{v\left(g\right)}$, où $v\left(g\right) \in E$, tel que $t_{v\left(g\right)}s_O\left(g\right) \in G$. \\

$v\left(g\right)$ est bien déterminé à un élément de $L$ près.

Soit $\left(g_1, g_2\right) \in \left(\overrightarrow{G}\right)^2$. Alors :

\begin{align*}
t_{v\left(g_1\right)} s_O\left(g_1\right) t_{v\left(g_2\right)} s_O\left(g_2\right) & = t_{v\left(g_1\right)} s_O\left(g_1\right)t_{v\left(g_2\right)} \left(s_O\left(g_1\right)^{-1} s_O\left(g_1\right)\right) s_O\left(g_2\right) \\
& = t_{v\left(g_1\right)} t_{\overrightarrow{s_O\left(g_1\right)}\left(v\left(g_2\right)\right)} s_O\left(g_1\right) s_O\left(g_2\right) \\
& = t_{v\left(g_1\right)} t_{g_1\left(v\left(g_2\right)\right)} s_O\left(g_1 g_2\right) \\
& = t_{v\left(g_1\right) + g_1\left(v\left(g_2\right)\right)} s_O\left(g_1 g_2\right) \\
& = t_{v\left(g_1 g_2\right)} s_O\left(g_1 g_2\right) \\
\end{align*}

Donc $v\left(g_1 g_2\right) = v\left(g_1\right) + g_1 v\left(g_2\right)$. \\

On obtient le cocycle $c_G : \overrightarrow{G} \longrightarrow E/L$ en posant $c_G\left(g\right) = $ image de $v\left(g\right)$ dans $E/L$. \\

Supposons que $G' = t_l G t_l^{-1}$. Alors on vérifie que $c_{G'} = c_G + $ un cobord. \\

Finalement, on montre que pour un cristal arithmétique $\left(\Gamma, L\right)$, on a une bijection 

{\hspace*{-1.5cm}
\begin{tikzcd}
\lb \mathrm{Classe \; de \; translation \; par\;} E\mathrm{\; de \; groupes \; cristallographiques \;} G \mathrm{\; tels \;que \;} \overrightarrow{G} = \Gamma\rb \arrow{r}{\sim} & H^{1}\left(\Gamma, E/L\right) \\
\end{tikzcd}
}
\vspace*{-1cm}

qui a un groupe $G$ associe le cocycle $c_G$. \\

En particulier, dans $H^1\left(\Gamma, E/L\right)$, on a l'élément nul. Celui-ci correspond à un groupe cristallographique distingué de cristal arithmétique $\left(\Gamma, L\right)$. Ce groupe distingué est le produit semi-direct : $L \rtimes \Gamma \overset{\sim}{\longrightarrow} \!\!\!\!\! \underbrace{t_L}_{\subseteq \; \mathrm{Isom}\left(\mathcal{E}\right)} \!\!\!\!\! \cdot \!\!\!\!\! \underbrace{s_O\left(\Gamma\right)}_{\; \; \; \; \; \; \; \subseteq \; \mathrm{Isom}\left(\mathcal{E}\right)} \subseteq \mathrm{Isom}\left(L\right)$, tel que $\overrightarrow{t_L \cdot s_O\left(\Gamma\right)} = \Gamma$. \\

\fi

\subsection{Calcul de cohomologies}

\begin{proposition} ~ \\

\begin{center}
\begin{tabular}{c|c|c}
$\Gamma$ & $H^1\left(\Gamma, E/L\right)$ & $\abs{H^1\left(\Gamma, E/L\right)}$ \\
\hline 
$C_1$ & $0$ & $1$ \\
$C_2$ & $0$ & $1$ \\
$C_3$ & $0$ & $1$ \\
$C_4$ & $0$ & $1$ \\
$C_6$ & $0$ & $1$ \\
$D_1$ & $\Z/2$ & $2$ \\
$D_{1^*}$ & $0$ & $1$ \\
$D_2$ & $\Z/2 \times \Z/2$ & $4$ \\
$D_{2^*}$ & $0$ & $1$ \\
$D_{3}$ & $0$ & $1$ \\
$D_{3^*}$ & $0$ & $1$ \\
$D_4$ & $\Z/2$ & $2$ \\
$D_{6}$ & $0$ & $1$ \\
\hline
$13$ & $\times$ & $18$ \\
\end{tabular}
\end{center}
\end{proposition}


\ifdefined\COMPLETE
\else
    \end{document}
\fi

