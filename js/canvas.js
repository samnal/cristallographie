function CanvasState(canvas, group){
  this.canvas = canvas;
  this.width = window.innerWidth -100;
  this.canvas.width = window.innerWidth -100;
  this.height = window.innerHeight -220;
  this.canvas.height = window.innerHeight -220;
  this.ctx = canvas.getContext('2d');
  this.scale = 100;
  this.group = group;

  //for style
  var stylePaddingLeft, stylePaddingTop, styleBorderLeft, styleBorderTop;
  if (document.defaultView && document.defaultView.getComputedStyle) {
    this.stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingLeft'], 10)      || 0;
    this.stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingTop'], 10)       || 0;
    this.styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderLeftWidth'], 10)  || 0;
    this.styleBorderTop   = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderTopWidth'], 10)   || 0;
  }
  var html = document.body.parentNode;
  this.htmlTop = html.offsetTop;
  this.htmlLeft = html.offsetLeft;


  this.bPressed = false;
  this.fPressed = false;
  this.rPressed = false;
  this.tPressed = true;
  this.iPressed = true;
  this.mouseIsPressed = false;

  this.currentLine = null; //line currently being drawn
  this.currentOffset = new Point(0,0); //where the last point of the current line has been drawn
  this.lines = [];  //lines that have been drawn
  this.removedLines = []; //undone lines

  var myState = this; //TACKLE LATER !!!!


//events
canvas.addEventListener('selectstart', function(e) { e.preventDefault(); return false; }, false);

canvas.addEventListener('mouseup', function(e){
  var m = myState.getMouse(e);
  myState.mouseIsPressed = false;

  if(myState.currentLine != null){
    if(!myState.bPressed){
      myState.addLine(myState.currentLine);
      myState.currentLine = null;
      myState.currentOffset = new Point(0,0);
    }
  }


});

canvas.addEventListener('mousedown', function(e){
  var m = myState.getMouse(e);
  myState.mouseIsPressed = true;

if(myState.currentLine==null){

  var n = new Point ((m.x - myState.width/2)/myState.scale, ((-m.y) + myState.height/2)/myState.scale);
  var pn = myState.group.centrer(n, myState);
  myState.currentLine = new Line( pn, pn);
  myState.currentOffset = n;
}else{
  //myState.bPressed = false;
  myState.addLine(myState.currentLine);
  myState.currentLine = null;
  myState.currentOffset = new Point(0,0);
}
},true);

canvas.addEventListener('mousemove', function(e){
  if(myState.currentLine !=null){
    var m1 = myState.getMouse(e);
    var m2 = new Point((m1.x - myState.width/2)/myState.scale, ((-m1.y) + myState.height/2)/myState.scale);
    var m = new Point(m2.x - myState.currentOffset.x, m2.y - myState.currentOffset.y);
    var pm = new Point(myState.currentLine.p1.x + m.x, myState.currentLine.p1.y + m.y);
    if(myState.mouseIsPressed){
      myState.currentLine.setP2( pm);
      myState.currentLine.setPC( pm);
    }else{
      if(myState.bPressed){
        myState.currentLine.setPC( pm);
      }
    }
  }
},true);


window.addEventListener('resize',function(e){
    myState.width  = window.innerWidth-100;
    myState.canvas.width  = window.innerWidth-100;
    myState.height = window.innerHeight-220;
    myState.canvas.height = window.innerHeight-220;

}, true);



//touch events
//each touchEvent override it's corresponding mouseEvent
canvas.addEventListener("touchstart", function (e) {
  mousePos = getTouchPos(canvas, e);
  var touch = e.touches[0];
  var mouseEvent = new MouseEvent("mousedown", {
    clientX: touch.clientX,
    clientY: touch.clientY
  });
  canvas.dispatchEvent(mouseEvent);
}, false);

canvas.addEventListener("touchend", function (e) {
  var mouseEvent = new MouseEvent("mouseup", {});
  canvas.dispatchEvent(mouseEvent);
}, false);

canvas.addEventListener("touchmove", function (e) {
  var touch = e.touches[0];
  var mouseEvent = new MouseEvent("mousemove", {
    clientX: touch.clientX,
    clientY: touch.clientY
  });
  canvas.dispatchEvent(mouseEvent);
}, false);


//The following suppress the usual activities of touch e.g. moving the page
document.body.addEventListener("touchstart", function (e) {
  if (e.target == canvas) {
    e.preventDefault();
  }
}, false);
document.body.addEventListener("touchend", function (e) {
  if (e.target == canvas) {
    e.preventDefault();
  }
}, false);
document.body.addEventListener("touchmove", function (e) {
  if (e.target == canvas) {
    e.preventDefault();
  }
}, false);


// refresh rate
this.interval = 30;
setInterval(function() { myState.draw(); }, myState.interval);

}

CanvasState.prototype.changeB = function () {
  this.bPressed = !this.bPressed;
};
CanvasState.prototype.changeF = function () {
  this.fPressed = !this.fPressed;
};
CanvasState.prototype.changeR = function() {
  this.rPressed = !this.rPressed;
}
CanvasState.prototype.changeT = function () {
  this.tPressed = !this.tPressed;
};
CanvasState.prototype.changeI = function () {
  this.iPressed = !this.iPressed;
};
CanvasState.prototype.changePlus = function () {
  if(this.scale < 200){
  this.scale = this.scale+25;
  }
};
CanvasState.prototype.changeMinus = function () {
  if(this.scale > 25){
    this.scale = this.scale-25;
  }
};


CanvasState.prototype.setGroup = function (group) {
  this.group = group;
};



CanvasState.prototype.addLine = function(line){
  this.lines.push(line);
}

CanvasState.prototype.undoLine = function(){
  if(this.currentLine != null){
    this.currentLine = null;
  }
  else{
    if(this.lines.length>0){
      this.removed.push(this.lines.pop());
    }
  }
}

CanvasState.prototype.rmAllLines = function(){
  this.removed = [];
  this.lines = [];
  this.currentLine = null;
}

CanvasState.prototype.redoLine = function(){
  if(this.removed.length>0){
    this.lines.push(this.removed.pop());
  }
}

CanvasState.prototype.clear = function() {
    this.ctx.clearRect(0, 0, this.width, this.height);
  }

  CanvasState.prototype.draw = function(){
      this.clear();

      if(this.fPressed){
        this.ctx.strokeStyle= 'blue';
        this.ctx.lineWidth = Math.ceil(this.scale/120);
        this.ctx.beginPath();
        var domain_draw;
        if(this.tPressed && this.iPressed){
          domain_draw = this.group.translate_reseau(this.group.apply(this.group.fun_dom),this.scale, this.width, this.height);
        }else if(this.tPressed){
          domain_draw = this.group.translate_reseau(this.group.fun_dom,this.scale, this.width, this.height);
        }else if(this.iPressed){
          domain_draw = this.group.apply(this.group.fun_dom);
        }else{
          domain_draw = this.group.fun_dom;
        }
        for(var i=0;i<domain_draw.length;i++){
          domain_draw[i].draw(this.ctx,this.scale, this.width, this.height);
        }
        this.ctx.stroke();
      }

      if(this.rPressed){
        this.ctx.strokeStyle= 'red';
        this.ctx.lineWidth = Math.ceil(this.scale/120);
        this.ctx.beginPath();
        var reseau_draw = this.group.translate_reseau(this.group.getReseau(),this.scale, this.width, this.height);
        for(var i=0;i<reseau_draw.length;i++){
          reseau_draw[i].draw(this.ctx,this.scale, this.width, this.height);
        }
        this.ctx.stroke();
      }



      this.ctx.strokeStyle= 'black';
      this.ctx.lineWidth = Math.ceil(this.scale/40);
      this.ctx.beginPath();
      var lines_draw;
      if(this.tPressed && this.iPressed){
        lines_draw = this.group.translate_reseau(this.group.apply(this.lines),this.scale, this.width, this.height);
      }else if(this.tPressed){
        lines_draw = this.group.translate_reseau(this.lines,this.scale, this.width, this.height);
      }else if(this.iPressed){
        lines_draw = this.group.apply(this.lines);
      }else{
        lines_draw = this.lines;
      }

      for(var i=0;i<lines_draw.length;i++){
        lines_draw[i].draw(this.ctx,this.scale, this.width, this.height);
      }
      if(this.currentLine){
        var line_draw;
        if(this.tPressed && this.iPressed){
          line_draw = this.group.translate_reseau(this.group.apply([this.currentLine]),this.scale, this.width, this.height);
        }else if(this.tPressed){
          line_draw = this.group.translate_reseau([this.currentLine],this.scale, this.width, this.height);
        }else if(this.iPressed){
          line_draw = this.group.apply([this.currentLine]);
        }else{
          line_draw = [this.currentLine];
        }
        for(var i=0;i<line_draw.length;i++){
          line_draw[i].draw(this.ctx,this.scale, this.width, this.height);
        }
      }
      this.ctx.stroke();
  }

CanvasState.prototype.getMouse = function(e){
  var element = this.canvas, offsetX = 0, offsetY = 0, mx, my;

  if(element.offsetParent != undefined){
    do{
      offsetX += element.offsetLeft;
      offsetY += element.offsetTop;
    } while((element = element.offsetParent));
  }
  offsetX += this.stylePaddingLeft + this.styleBorderLeft + this.htmlLeft;
  offsetY += this.stylePaddingTop + this.styleBorderTop + this.htmlTop;
  return new Point (
    e.pageX - offsetX,
    e.pageY - offsetY
  );
}

function getTouchPos(canvasDom, touchEvent) {
  var rect = canvasDom.getBoundingClientRect();
  return new Point(
    touchEvent.touches[0].clientX - rect.left,
    touchEvent.touches[0].clientY - rect.top
  );
}
