function Isom(x,y,a11,a12,a21,a22){
  this.x = x;
  this.y = y;
  this.a11 = a11;
  this.a12 = a12;
  this.a21 = a21;
  this.a22 = a22;
}

//apply the isom to the given point
Isom.prototype.applyPoint = function (p) {
  return new Point(this.x+p.x*this.a11+p.y*this.a21, this.y+p.x*this.a12+p.y*this.a22);
}

//apply the isom to the given line
Isom.prototype.applyLine = function (l) {
  return new Line(this.applyPoint(l.p1), this.applyPoint(l.p2), this.applyPoint(l.pC));
}
