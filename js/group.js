function Group(str, rX, rY, isoms, d){
  this.name = str;
  this.reseauX = rX; //x translation
  this.reseauY = rY; //y translation
  this.dX = Math.sqrt(Math.pow(this.reseauX[0],2)+Math.pow(this.reseauX[1],2));
  this.dY = Math.sqrt(Math.pow(this.reseauY[0],2)+Math.pow(this.reseauY[1],2));
  this.isoms = isoms; //array of the isoms
  this.fun_dom = [];
  for(var i=0; i<(d.length-1); i++){
    this.fun_dom.push(new Line(new Point(d[i+1][0], d[i+1][1]), new Point(d[i][0], d[i][1])));
  }
}

Group.prototype.apply = function (lines) {
  var res = [];
  for (var i = 0; i < lines.length; i++) {
    for (var j = 0; j < this.isoms.length; j++) {
      res.push(this.isoms[j].applyLine(lines[i]));
    }
  }
  return res;

}

Group.prototype.translate_reseau = function (lines, scale, width, height) {
  var nbX = Math.ceil(width/(this.reseauX[0]*scale));
  var nbY = Math.ceil(height/(this.reseauY[1]*scale));
  var decX, decY;
  var l;
  var lines_res = [];

  for (var i = 0; i < lines.length; i++) {
    l = lines[i];
    for(var j = -2*nbX; j < 2*nbX; j++){
      for(var k = -2*nbX; k< 2*nbY; k++){
        decX = j*this.reseauX[0]+k*this.reseauY[0];
        decY = j*this.reseauX[1]+k*this.reseauY[1];
        lines_res.push(new Line(new Point(l.p1.x+decX, l.p1.y+decY), new Point(l.p2.x+decX, l.p2.y+decY), new Point(l.pC.x+decX, l.pC.y+decY)));
      }
    }
  }
  return lines_res;
};

Group.prototype.centrer = function (p, ms) {
  //writing p in the new base
  var det = this.reseauX[0]*this.reseauY[1] - this.reseauX[1]*this.reseauY[0];
  var a = (p.x*this.reseauY[1] - p.y*this.reseauY[0])/det;
  var b = (p.y*this.reseauX[0] - p.x*this.reseauX[1])/det;
  var a_ = Math.floor(a),
      b_ = Math.floor(b);
  var np = new Point(p.x - a_*this.reseauX[0] - b_*this.reseauY[0], p.y - a_*this.reseauX[1] - b_*this.reseauY[1]);
  return np;

};

Group.prototype.getReseau = function () {
  return [new Line(new Point(0, 0), new Point(this.reseauX[0], this.reseauX[1]), new Point(this.reseauX[0], this.reseauX[1])), new Line(new Point(0, 0), new Point(this.reseauY[0], this.reseauY[1]), new Point(this.reseauY[0], this.reseauY[1]))];
};
