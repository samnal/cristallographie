function initialisator(can, selector, url) {
  var start = url.indexOf("?") + 1;
  var end = url.length + 1;
  var sub_url = url.slice(start, end - 1);
  var raw = sub_url.split("&");
  var pts;

  if (sub_url === url || sub_url === "" || raw.length<1){
    return 'p1';
  }
  if(raw[0]== '0'){
    document.getElementById("save").style.display = 'none';
  }
  selector.setGroup(raw[0]);
  for (var i=1; i<raw.length; i++) {
    pts = raw[i].split("\_");
    if(pts.length != 6){
      continue;
    }else{
      can.addLine(new Line(new Point(parseFloat(pts[0]), parseFloat(pts[1])), new Point(parseFloat(pts[2]), parseFloat(pts[3])), new Point(parseFloat(pts[4]), parseFloat(pts[5]))));
    }
  }
  return raw[0];
}
