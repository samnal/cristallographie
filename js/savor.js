function Savor(can, url){
  var index_quest = url.indexOf("?");
  this.sub_url = "";
  this.can = can;

  if(index_quest >=0 ){
    this.sub_url = url.slice(0, index_quest);
  }else{
    this.sub_url = url;
  }
}

Savor.prototype.save = function () {
  var return_url = this.sub_url + "?";
  return_url += this.can.group.name;
  for(var i=0; i<this.can.lines.length; i++){
    return_url += "&" + can.lines[i].toString();
  }
  window.location.href = return_url;
}
