//Constructor for Line objects to hold data for all drawn objects.
//with one control point pC
function Line(p1, p2, pC){
  this.p1 = p1;
  this.p2 = p2;
  this.pC = pC || p2;
}

/*Draws this line to a given getContext.
 *The origin is considered to be the center of the canvas but the 'real'
 * origine is (-width/2, height/2)
 * i.e. every point is enter like this :
 * new Point (x - myState.width/2)/myState.scale, ((-y) + myState.height/2)/myState.scale)
 */
Line.prototype.draw = function(ctx, scale, width, height){
  ctx.moveTo(this.p1.x*scale + width/2, -  (this.p1.y*scale - height/2));
  ctx.quadraticCurveTo(this.pC.x*scale + width/2, - (this.pC.y*scale - height/2),
    this.p2.x*scale + width/2, - (this.p2.y*scale - height/2));
}

//set functions
Line.prototype.setP1 = function(p){
  this.p1 = p;
}
Line.prototype.setP2 = function(p){
  this.p2 = p;
}
Line.prototype.setPC = function(p){
  this.pC = p;
}

Line.prototype.toString = function () {
  return this.p1.toString()+"\_"+this.p2.toString()+"\_"+this.pC.toString();

};
